#include "LCCInstrInfo.h"
#include "LCCMCInstLower.h"
#include "LCCTargetMachine.h"
#include "MCTargetDesc/LCCInstPrinter.h"
#include "MCTargetDesc/LCCMCTargetDesc.h"
#include "TargetInfo/LCCTargetInfo.h"
#include "llvm/CodeGen/AsmPrinter.h"
#include "llvm/CodeGen/MachineInstr.h"
#include "llvm/CodeGen/MachineModuleInfoImpls.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/TargetLoweringObjectFileImpl.h"
#include "llvm/IR/Mangler.h"
#include "llvm/MC/MCAsmInfo.h"
#include "llvm/MC/MCContext.h"
#include "llvm/MC/MCInst.h"
#include "llvm/MC/MCInstBuilder.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/MC/MCSymbol.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/raw_ostream.h"
using namespace llvm;
#define DEBUG_TYPE "asm-printer"
namespace {
class LCCAsmPrinter : public AsmPrinter {
public:
  explicit LCCAsmPrinter(TargetMachine &TM,
                          std::unique_ptr<MCStreamer> Streamer)
      : AsmPrinter(TM, std::move(Streamer)) {}
  StringRef getPassName() const override { return "LCC Assembly Printer"; }
  bool PrintAsmOperand(const MachineInstr *MI, unsigned OpNo,
                       const char *ExtraCode, raw_ostream &OS) override;
  void emitInstruction(const MachineInstr *MI) override;
};
} // end of anonymous namespace

bool LCCAsmPrinter::PrintAsmOperand(const MachineInstr *MI, unsigned OpNo,
                                     const char *ExtraCode, raw_ostream &OS) {
  if (ExtraCode)
    return AsmPrinter::PrintAsmOperand(MI, OpNo, ExtraCode, OS);
  LCCMCInstLower Lower(MF->getContext(), *this);
  MCOperand MO(Lower.lowerOperand(MI->getOperand(OpNo)));
  LCCInstPrinter::printOperand(MO, MAI, OS);
  return false;
}

void LCCAsmPrinter::emitInstruction(const MachineInstr *MI) {
  MCInst LoweredMI;
  switch (MI->getOpcode()) {
  // case LCC::RET:
  //   LoweredMI = MCInstBuilder(LCC::JMP).addReg(LCC::LR);
  //   break;
  default:
    LCCMCInstLower Lower(MF->getContext(), *this);
    Lower.lower(MI, LoweredMI);
    break;
  }
  EmitToStreamer(*OutStreamer, LoweredMI);
}

extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeLCCAsmPrinter() {
  RegisterAsmPrinter<LCCAsmPrinter> X(getTheLCCTarget());
}
