//===-- LCCISelDAGToDAG.h -*- C++ -*-===//
#ifndef LLVM_LIB_TARGET_LCC_LCCISELDAGTODAG_H
#define LLVM_LIB_TARGET_LCC_LCCISELDAGTODAG_H
#include "llvm/Support/CodeGen.h"
namespace llvm {
  class LCCTargetMachine;
  class FunctionPass;
  FunctionPass *createLCCISelDag(LCCTargetMachine &TM,
                                  CodeGenOpt::Level OptLevel);
} // end namespace llvm

#endif /* LLVM_LIB_TARGET_LCC_LCCISELDAGTODAG_H */
