#ifndef LLVM_LIB_TARGET_LCC_LCCTARGETMACHINE_H
#define LLVM_LIB_TARGET_LCC_LCCTARGETMACHINE_H
#include "LCCSubtarget.h"
#include "llvm/Target/TargetLoweringObjectFile.h"
#include "llvm/Target/TargetMachine.h"
namespace llvm {
class LCCTargetMachine : public LLVMTargetMachine {
  std::unique_ptr<TargetLoweringObjectFile> TLOF;
  mutable StringMap<std::unique_ptr<LCCSubtarget>> SubtargetMap;

public:
  LCCTargetMachine(const Target &T, const Triple &TT, StringRef CPU,
                    StringRef FS, const TargetOptions &Options,
                    Optional<Reloc::Model> RM, Optional<CodeModel::Model> CM,
                    CodeGenOpt::Level OL, bool JIT);
  ~LCCTargetMachine() override;
  const LCCSubtarget *getSubtargetImpl(const Function &) const override;
  TargetPassConfig *createPassConfig(PassManagerBase &PM) override;
  TargetLoweringObjectFile *getObjFileLowering() const override {
    return TLOF.get();
  }
};

} // end namespace llvm

#endif
