//===-- LCCBaseInfo.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef MCTARGETDESC_LCCBASEINFO_H
#define MCTARGETDESC_LCCBASEINFO_H

namespace llvm {
  namespace LCC {
    /// Target Operand Flag enum.
    enum TOF {
      //===------------------------------------------------------------------===//
      // LCC Specific MachineOperand flags.

      MO_NO_FLAG,
      /// MO_GOT - Represents the offset into the global offset table at which
      /// the address the relocation entry symbol resides during execution.
      MO_GOT,
      /// MO_GOT_CALL - Represents the offset into the global offset table at
      /// which the address of a call site relocation entry symbol resides
      /// during execution. This is different from the above since this flag
      /// can only be present in call instructions.
      MO_GOT_CALL,

      /// MO_GPREL - Represents the offset from the current gp value to be used
      /// for the relocatable object file being produced.
      MO_GPREL,

      /// MO_ABS_HI/LO - Represents the hi or low part of an absolute symbol
      /// address.
      MO_ABS_HI,
      MO_ABS_LO,

      /// MO_TLSGD - Represents the offset into the global offset table at which
      // the module ID and TSL block offset reside during execution (General
      // Dynamic TLS).
      MO_TLSGD,

      /// MO_TLSLDM - Represents the offset into the global offset table at which
      // the module ID and TSL block offset reside during execution (Local
      // Dynamic TLS).
      MO_TLSLDM,
      MO_DTP_HI,
      MO_DTP_LO,

      /// MO_GOTTPREL - Represents the offset from the thread pointer (Initial
      // Exec TLS).
      MO_GOTTPREL,

      /// MO_TPREL_HI/LO - Represents the hi and low part of the offset from
      // the thread pointer (Local Exec TLS).
      MO_TP_HI,
      MO_TP_LO,

      /// MO_GOT_HI16/LO16 - Relocations used for large GOTs.
      MO_GOT_HI16,
      MO_GOT_LO16
    }; // enum TOF {

  } // namespace LCC
} // namespace llvm

#endif /* MCTARGETDESC_LCCBASEINFO_H */
