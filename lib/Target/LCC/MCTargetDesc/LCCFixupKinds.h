//===-- LCCFixupKinds.h - LCC Specific Fixup Entries ----------*- C++ -*-===//
#ifndef LLVM_LIB_TARGET_LCC_MCTARGETDESC_LCCFIXUPKINDS_H
#define LLVM_LIB_TARGET_LCC_MCTARGETDESC_LCCFIXUPKINDS_H
#include "llvm/MC/MCFixup.h"

namespace llvm {
  namespace LCC {
    // Although most of the current fixup types reflect a unique relocation
    // one can have multiple fixup types for a given relocation and thus need
    // to be uniquely named.
    //
    // This table *must* be in the save order of
    // MCFixupKindInfo Infos[LCC::NumTargetFixupKinds]
    // in LCCAsmBackend.cpp.
    //@Fixups {
    enum Fixups {
      //@ Pure upper 32 bit fixup resulting in - R_LCC_32.
      fixup_LCC_32 = FirstTargetFixupKind,

      // Pure upper 16 bit fixup resulting in - R_LCC_HI16.
      fixup_LCC_HI16,

      // Pure lower 16 bit fixup resulting in - R_LCC_LO16.
      fixup_LCC_LO16,

      // 16 bit fixup for GP offest resulting in - R_LCC_GPREL16.
      fixup_LCC_GPREL16,

      // GOT (Global Offset Table)
      // Symbol fixup resulting in - R_LCC_GOT16.
      fixup_LCC_GOT,

      // PC relative branch fixup resulting in - R_LCC_PC16.
      // lcc PC16, e.g. beq
      fixup_LCC_PC16,

      // PC relative branch fixup resulting in - R_LCC_PC9.
      // lcc PC24, e.g. jeq, jmp
      fixup_LCC_PC9,
      // resulting in - R_LCC_CALL16.
      fixup_LCC_CALL16,
      // resulting in - R_LCC_TLS_GD.
      fixup_LCC_TLSGD,

      // resulting in - R_LCC_TLS_GOTTPREL.
      fixup_LCC_GOTTPREL,

      // resulting in - R_LCC_TLS_TPREL_HI16.
      fixup_LCC_TP_HI,

      // resulting in - R_LCC_TLS_TPREL_LO16.
      fixup_LCC_TP_LO,

      // resulting in - R_LCC_TLS_LDM.
      fixup_LCC_TLSLDM,

      // resulting in - R_LCC_TLS_DTP_HI16.
      fixup_LCC_DTP_HI,

      // resulting in - R_LCC_TLS_DTP_LO16.
      fixup_LCC_DTP_LO,
      // resulting in - R_LCC_GOT_HI16
      fixup_LCC_GOT_HI16,

      // resulting in - R_LCC_GOT_LO16
      fixup_LCC_GOT_LO16,

      // Marker
      LastTargetFixupKind,
      NumTargetFixupKinds = LastTargetFixupKind - FirstTargetFixupKind
    };
  } // namespace LCC
} // namespace llvm

#endif // LLVM_LCC_LCCFIXUPKINDS_H
