#ifndef LLVM_LIB_TARGET_LCC_MCTARGETDESC_LCCMCTARGETDESC_H
#define LLVM_LIB_TARGET_LCC_MCTARGETDESC_LCCMCTARGETDESC_H
#include <memory>
namespace llvm {
class MCAsmBackend;
class MCCodeEmitter;
class MCContext;
class MCInstrInfo;
class MCObjectTargetWriter;
class MCRegisterInfo;
class MCSubtargetInfo;
class MCTargetOptions;
class StringRef;
class Target;
class Triple;
class raw_pwrite_stream;
class raw_ostream;

MCCodeEmitter *createLCCMCCodeEmitter(const MCInstrInfo &MCII,
                                       const MCRegisterInfo &MRI,
                                       MCContext &Ctx);

MCAsmBackend *createLCCMCAsmBackend(const Target &T,
                                     const MCSubtargetInfo &STI,
                                     const MCRegisterInfo &MRI,
                                     const MCTargetOptions &Options);

std::unique_ptr<MCObjectTargetWriter> createLCCObjectWriter(uint8_t OSABI);
} // end namespace llvm

// Defines symbolic names for LCC registers.
// This defines a mapping from register name to register number.
#define GET_REGINFO_ENUM
#include "LCCGenRegisterInfo.inc"

namespace llvm {
namespace LCC {
enum { R5 = LCC::FP, R6 = LCC::SP, R7 = LCC::LR };
} // namespace LCC
} // namespace llvm

// Defines symbolic names for the LCC instructions.
#define GET_INSTRINFO_ENUM
#include "LCCGenInstrInfo.inc"

#define GET_SUBTARGETINFO_ENUM
#include "LCCGenSubtargetInfo.inc"

#endif
