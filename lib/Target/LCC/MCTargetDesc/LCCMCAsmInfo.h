#ifndef LLVM_LIB_TARGET_LCC_MCTARGETDESC_LCCMCASMINFO_H
#define LLVM_LIB_TARGET_LCC_MCTARGETDESC_LCCMCASMINFO_H
#include "llvm/MC/MCAsmInfoELF.h"
namespace llvm {
class Triple;
// define asm directives and similar asm information
class LCCMCAsmInfo : public MCAsmInfoELF {
public:
  explicit LCCMCAsmInfo(const Triple &TT);
};
} // end namespace llvm

#endif
