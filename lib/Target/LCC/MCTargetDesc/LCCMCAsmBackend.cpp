#include "MCTargetDesc/LCCFixupKinds.h"
#include "MCTargetDesc/LCCMCTargetDesc.h"
#include "llvm/MC/MCAsmBackend.h"
#include "llvm/MC/MCELFObjectWriter.h"
#include "llvm/MC/MCFixupKindInfo.h"
#include "llvm/MC/MCInst.h"
#include "llvm/MC/MCObjectWriter.h"
#include "llvm/MC/MCSubtargetInfo.h"
using namespace llvm;
namespace {
class LCCMCAsmBackend : public MCAsmBackend {
  uint8_t OSABI;

public:
  LCCMCAsmBackend(uint8_t osABI) : MCAsmBackend(support::big), OSABI(osABI) {}
  unsigned getNumFixupKinds() const override { return LCC::NumTargetFixupKinds; }
  const MCFixupKindInfo &getFixupKindInfo(MCFixupKind Kind) const override;
  void applyFixup(const MCAssembler &Asm, const MCFixup &Fixup,
                  const MCValue &Target, MutableArrayRef<char> Data,
                  uint64_t Value, bool IsResolved,
                  const MCSubtargetInfo *STI) const override;
  bool mayNeedRelaxation(const MCInst &Inst,
                         const MCSubtargetInfo &STI) const override {
    return false;
  }
  bool fixupNeedsRelaxation(const MCFixup &Fixup, uint64_t Value,
                            const MCRelaxableFragment *Fragment,
                            const MCAsmLayout &Layout) const override {
    return false;
  }
  bool writeNopData(raw_ostream &OS, uint64_t Count) const override;
  std::unique_ptr<MCObjectTargetWriter>
  createObjectTargetWriter() const override {
    return createLCCObjectWriter(OSABI);
  }
};
} // end anonymous namespace

const MCFixupKindInfo &
LCCMCAsmBackend::getFixupKindInfo(MCFixupKind Kind) const {
  unsigned JSUBReloRec = 0;
  JSUBReloRec = MCFixupKindInfo::FKF_IsPCRel | MCFixupKindInfo::FKF_Constant;
  const static MCFixupKindInfo Infos[LCC::NumTargetFixupKinds] = {
      // This table *must* be in same the order of fixup_* kinds in
      // Cpu0FixupKinds.h.
      //
      // name                        offset  bits  flags
      {"fixup_LCC_32", 0, 32, 0},
      {"fixup_LCC_HI16", 0, 16, 0},
      {"fixup_LCC_LO16", 0, 16, 0},
      {"fixup_LCC_GPREL16", 0, 16, 0},
      {"fixup_LCC_GOT", 0, 16, 0},
      {"fixup_LCC_PC16", 0, 16, MCFixupKindInfo::FKF_IsPCRel},
      {"fixup_LCC_PC24", 0, 24, JSUBReloRec},
      {"fixup_LCC_CALL16", 0, 16, 0},
      {"fixup_LCC_TLSGD", 0, 16, 0},
      {"fixup_LCC_GOTTP", 0, 16, 0},
      {"fixup_LCC_TP_HI", 0, 16, 0},
      {"fixup_LCC_TP_LO", 0, 16, 0},
      {"fixup_LCC_TLSLDM", 0, 16, 0},
      {"fixup_LCC_DTP_HI", 0, 16, 0},
      {"fixup_LCC_DTP_LO", 0, 16, 0},
      {"fixup_LCC_GOT_HI16", 0, 16, 0},
      {"fixup_LCC_GOT_LO16", 0, 16, 0}};

  if (Kind < FirstTargetFixupKind)
    return MCAsmBackend::getFixupKindInfo(Kind);

  assert(unsigned(Kind - FirstTargetFixupKind) < getNumFixupKinds() &&
         "Invalid kind!");
  return Infos[Kind - FirstTargetFixupKind];
}

void LCCMCAsmBackend::applyFixup(const MCAssembler &Asm, const MCFixup &Fixup,
                                 const MCValue &Target,
                                 MutableArrayRef<char> Data, uint64_t Value,
                                 bool IsResolved,
                                 const MCSubtargetInfo *STI) const {}

bool LCCMCAsmBackend::writeNopData(raw_ostream &OS, uint64_t Count) const {
  for (uint64_t I = 0; I != Count; ++I)
    OS << '\x7';
  return true;
}

MCAsmBackend *llvm::createLCCMCAsmBackend(const Target &T,
                                          const MCSubtargetInfo &STI,
                                          const MCRegisterInfo &MRI,
                                          const MCTargetOptions &Options) {
  uint8_t OSABI =
      MCELFObjectTargetWriter::getOSABI(STI.getTargetTriple().getOS());
  return new LCCMCAsmBackend(OSABI);
}
