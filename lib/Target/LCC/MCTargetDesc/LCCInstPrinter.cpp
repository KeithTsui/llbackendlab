#include "LCCInstPrinter.h"
#include "llvm/MC/MCAsmInfo.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCInst.h"
#include "llvm/MC/MCSymbol.h"
#include "llvm/Support/Casting.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/MathExtras.h"
#include "llvm/Support/raw_ostream.h"
#include <cassert>
#include <cstdint>

using namespace llvm;

#define DEBUG_TYPE "asm-printer"

#include "LCCGenAsmWriter.inc"

void LCCInstPrinter::printOperand(const MCInst *MI, uint64_t Address, int OpNum,
                                  const MCSubtargetInfo &STI, raw_ostream &O) {
  printOperand(MI, OpNum, STI, O);
}

void LCCInstPrinter::printOperand(const MCInst *MI, int OpNum,
                                  const MCSubtargetInfo &STI, raw_ostream &O) {
  const MCOperand &MO = MI->getOperand(OpNum);
  if (MO.isReg()) {
    if (!MO.getReg())
      O << '0';
    else
      O << '%' << getRegisterName(MO.getReg());
  } else if (MO.isImm())
    O << MO.getImm();
  else if (MO.isExpr())
    MO.getExpr()->print(O, &MAI);
  else
    llvm_unreachable("Invalid operand");
}

void LCCInstPrinter::printOperand(const MCOperand &MO, const MCAsmInfo *MAI,
                                  raw_ostream &O) {
  if (MO.isReg()) {
    if (!MO.getReg())
      O << '0';
    else
      O << '%' << getRegisterName(MO.getReg());
  } else if (MO.isImm())
    O << MO.getImm();
  else if (MO.isExpr())
    MO.getExpr()->print(O, MAI);
  else
    llvm_unreachable("Invalid operand");
}

void LCCInstPrinter::printImmOperand(const MCInst *MI, int OpNum,
                                     const MCSubtargetInfo &STI,
                                     raw_ostream &O) {
  int64_t Value = MI->getOperand(OpNum).getImm();
  O << Value;
}

void LCCInstPrinter::printMemOperand(const MCInst *MI, unsigned opNo,
                                     const MCSubtargetInfo &STI,
                                     raw_ostream &O) {
  printOperand(MI, opNo, O);
  O << ", ";
  printOperand(MI, opNo + 1, O);
}

void LCCInstPrinter::printPCRelOperand(const MCInst *MI, uint64_t Address,
                                       int OpNum, const MCSubtargetInfo &STI,
                                       raw_ostream &O) {
  // TODO
  const MCOperand &MO = MI->getOperand(OpNum);
  if (MO.isImm()) {
    O << "0x";
    O.write_hex(MO.getImm());
  } else
    MO.getExpr()->print(O, &MAI);
}

void LCCInstPrinter::printOperand(const MCInst *MI, unsigned OpNo,
                                  raw_ostream &O) {
  const MCOperand &Op = MI->getOperand(OpNo);
  if (Op.isReg()) {
    auto const *regName = getRegisterName(Op.getReg());
    O << "%" << regName;
    return;
  }
  if (Op.isImm()) {
    O << Op.getImm();
    return;
  }
  assert(Op.isExpr() && "unknown operand kind in printOperand");
  Op.getExpr()->print(O, &MAI, true);
}

void LCCInstPrinter::printInst(const MCInst *MI, uint64_t Address,
                               StringRef Annot, const MCSubtargetInfo &STI,
                               raw_ostream &O) {
  printInstruction(MI, Address, STI, O);
  printAnnotation(O, Annot);
}
