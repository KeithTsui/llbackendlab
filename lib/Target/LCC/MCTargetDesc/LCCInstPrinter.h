#ifndef LLVM_LIB_TARGET_LCC_MCTARGETDESC_LCCINSTPRINTER_H
#define LLVM_LIB_TARGET_LCC_MCTARGETDESC_LCCINSTPRINTER_H
#include "llvm/MC/MCInstPrinter.h"
#include <_types/_uint64_t.h>
#include <cstdint>
namespace llvm {
class MCAsmInfo;
class MCOperand;

// This class prints a LCC MCInst to a .s file.
class LCCInstPrinter : public MCInstPrinter {
public:
  LCCInstPrinter(const MCAsmInfo &MAI, const MCInstrInfo &MII,
                 const MCRegisterInfo &MRI)
      : MCInstPrinter(MAI, MII, MRI) {}

  static const char *getRegisterName(unsigned RegNo);

  static void printOperand(const MCOperand &MO, const MCAsmInfo *MAI,
                           raw_ostream &O);
  void printOperand(const MCInst *MI, int OpNum, const MCSubtargetInfo &STI,
                    raw_ostream &O);

  void printOperand(const MCInst *MI, uint64_t Address,
                    int OpNum, const MCSubtargetInfo &STI,
                    raw_ostream &O);

  void printOperand(const MCInst *MI, unsigned OpNo, raw_ostream &O);

  std::pair<const char *, uint64_t> getMnemonic(const MCInst *MI) override;
  void printInst(const MCInst *MI, uint64_t Address, StringRef Annot,
                 const MCSubtargetInfo &STI, raw_ostream &O) override;
  void printInstruction(const MCInst *MI, uint64_t Address,
                        const MCSubtargetInfo &STI, raw_ostream &O);

  void printImmOperand(const MCInst *MI, int OpNum, const MCSubtargetInfo &STI,
                       raw_ostream &O);
  void printMemOperand(const MCInst *MI, unsigned opNo,
                       const MCSubtargetInfo &STI, raw_ostream &O);
  void printPCRelOperand(const MCInst *MI, uint64_t Address, int OpNum,
                         const MCSubtargetInfo &STI, raw_ostream &O);
};

} // end namespace llvm

#endif // LLVM_LIB_TARGET_LCC_MCTARGETDESC_LCCINSTPRINTER_H
