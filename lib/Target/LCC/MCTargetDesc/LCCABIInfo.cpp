//===-- LCCABIInfo.cpp -*- C++ -*-===//

#include "LCCABIInfo.h"
#include "LCCMCTargetDesc.h"
#include "LCCRegisterInfo.h"

namespace llvm {
namespace LCCABIInfo {
/// The registers to use for byval arguments.
ArrayRef<MCPhysReg> const GetByValArgRegs() { return {}; }

/// The registers to use for the variable argument list.
ArrayRef<MCPhysReg> const GetVarArgRegs() { return {}; }

/// Obtain the size of the area allocated by the callee for arguments.
/// CallingConv::FastCall affects the value for O32.
unsigned GetCalleeAllocdArgSizeInBytes(CallingConv::ID CC) { return 0; }
unsigned GetStackPtr() { return LCC::SP; }
unsigned GetFramePtr() { return LCC::FP; }
unsigned GetEhDataReg(unsigned I) { return 0; }
int EhDataRegSize() { return 0; }

} // namespace LCCABIInfo
} // namespace llvm
