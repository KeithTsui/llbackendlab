//===-- LCCMCExpr.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef MCTARGETDESC_LCCMCEXPR_H
#define MCTARGETDESC_LCCMCEXPR_H

#include "llvm/MC/MCAsmLayout.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCValue.h"

namespace llvm {

class LCCMCExpr : public MCTargetExpr {
public:
  enum LCCExprKind {
    CEK_None,
    CEK_ABS_HI,
    CEK_ABS_LO,
    CEK_CALL_HI16,
    CEK_CALL_LO16,
    CEK_DTP_HI,
    CEK_DTP_LO,
    CEK_GOT,
    CEK_GOTTPREL,
    CEK_GOT_CALL,
    CEK_GOT_DISP,
    CEK_GOT_HI16,
    CEK_GOT_LO16,
    CEK_GPREL,
    CEK_TLSGD,
    CEK_TLSLDM,
    CEK_TP_HI,
    CEK_TP_LO,
    CEK_Special,
  };

private:
  const LCCExprKind Kind;
  const MCExpr *Expr;

  explicit LCCMCExpr(LCCExprKind Kind, const MCExpr *Expr)
      : Kind(Kind), Expr(Expr) {}

public:
  static const LCCMCExpr *create(LCCExprKind Kind, const MCExpr *Expr,
                                  MCContext &Ctx);
  static const LCCMCExpr *
  create(const MCSymbol *Symbol, LCCMCExpr::LCCExprKind Kind, MCContext &Ctx);
  static const LCCMCExpr *createGpOff(LCCExprKind Kind, const MCExpr *Expr,
                                       MCContext &Ctx);

  /// Get the kind of this expression.
  LCCExprKind getKind() const { return Kind; }

  /// Get the child of this expression.
  const MCExpr *getSubExpr() const { return Expr; }

  void printImpl(raw_ostream &OS, const MCAsmInfo *MAI) const override;
  bool evaluateAsRelocatableImpl(MCValue &Res, const MCAsmLayout *Layout,
                                 const MCFixup *Fixup) const override;
  void visitUsedExpr(MCStreamer &Streamer) const override;
  MCFragment *findAssociatedFragment() const override {
    return getSubExpr()->findAssociatedFragment();
  }

  void fixELFSymbolsInTLSFixups(MCAssembler &Asm) const override;

  static bool classof(const MCExpr *E) {
    return E->getKind() == MCExpr::Target;
  }

  bool isGpOff(LCCExprKind &Kind) const;
  bool isGpOff() const {
    LCCExprKind Kind;
    return isGpOff(Kind);
  }
};
} // namespace llvm

#endif /* MCTARGETDESC_LCCMCEXPR_H */
