#include "LCCMCTargetDesc.h"
#include "LCCInstPrinter.h"
#include "LCCMCAsmInfo.h"
#include "TargetInfo/LCCTargetInfo.h"
#include "llvm/MC/MCDwarf.h"
#include "llvm/MC/MCInstrInfo.h"
#include "llvm/MC/MCRegisterInfo.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/MC/MCSubtargetInfo.h"
#include "llvm/Support/TargetRegistry.h"

using namespace llvm;

#define GET_INSTRINFO_MC_DESC
#include "LCCGenInstrInfo.inc"

#define GET_SUBTARGETINFO_MC_DESC
#include "LCCGenSubtargetInfo.inc"

#define GET_REGINFO_MC_DESC
#include "LCCGenRegisterInfo.inc"

static MCAsmInfo *createLCCMCAsmInfo(const MCRegisterInfo &MRI,
                                      const Triple &TT,
                                      const MCTargetOptions &Options) {
  MCAsmInfo *MAI = new LCCMCAsmInfo(TT);
  return MAI;
}

static MCInstrInfo *createLCCMCInstrInfo() {
  MCInstrInfo *X = new MCInstrInfo();
  InitLCCMCInstrInfo(X);
  return X;
}

static MCRegisterInfo *createLCCMCRegisterInfo(const Triple &TT) {
  MCRegisterInfo *X = new MCRegisterInfo();
  InitLCCMCRegisterInfo(X, LCC::LR);
  return X;
}

static MCSubtargetInfo *createLCCMCSubtargetInfo(const Triple &TT,
                                                  StringRef CPU, StringRef FS) {
  return createLCCMCSubtargetInfoImpl(TT, CPU, /*TuneCPU*/ CPU, FS);
}

static MCInstPrinter *createLCCMCInstPrinter(const Triple &T,
                                              unsigned SyntaxVariant,
                                              const MCAsmInfo &MAI,
                                              const MCInstrInfo &MII,
                                              const MCRegisterInfo &MRI) {
  return new LCCInstPrinter(MAI, MII, MRI);
}

extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeLCCTargetMC() {
  // Register the MCAsmInfo.
  TargetRegistry::RegisterMCAsmInfo(getTheLCCTarget(), createLCCMCAsmInfo);

  // Register the MCInstrInfo.
  TargetRegistry::RegisterMCInstrInfo(getTheLCCTarget(),
                                      createLCCMCInstrInfo);

  // Register the MCRegisterInfo.
  TargetRegistry::RegisterMCRegInfo(getTheLCCTarget(),
                                    createLCCMCRegisterInfo);

  // Register the MCSubtargetInfo.
  TargetRegistry::RegisterMCSubtargetInfo(getTheLCCTarget(),
                                          createLCCMCSubtargetInfo);

  // Register the MCAsmBackend.
  TargetRegistry::RegisterMCAsmBackend(getTheLCCTarget(),
                                       createLCCMCAsmBackend);

  // Register the MCCodeEmitter.
  TargetRegistry::RegisterMCCodeEmitter(getTheLCCTarget(),
                                        createLCCMCCodeEmitter);

  // Register the MCInstPrinter.
  TargetRegistry::RegisterMCInstPrinter(getTheLCCTarget(),
                                        createLCCMCInstPrinter);
}
