#include "LCCSubtarget.h"
using namespace llvm;
#define DEBUG_TYPE "lcc-subtarget"
#define GET_SUBTARGETINFO_TARGET_DESC
#define GET_SUBTARGETINFO_CTOR
#include "LCCGenSubtargetInfo.inc"
void LCCSubtarget::anchor() {}
LCCSubtarget::LCCSubtarget(const Triple &TT, const std::string &CPU,
                             const std::string &FS, const TargetMachine &TM)
    : LCCGenSubtargetInfo(TT, CPU, /*TuneCPU*/ CPU, FS), TargetTriple(TT),
      InstrInfo(*this), TLInfo(TM, *this), FrameLowering() {}
