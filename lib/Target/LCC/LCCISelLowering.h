#ifndef LLVM_LIB_TARGET_LCC_LCCISELLOWERING_H
#define LLVM_LIB_TARGET_LCC_LCCISELLOWERING_H
#include "MCTargetDesc/LCCBaseInfo.h"
#include "MCTargetDesc/LCCMCTargetDesc.h"
#include "llvm/CodeGen/CallingConvLower.h"
#include "llvm/CodeGen/MachineBasicBlock.h"
#include "llvm/CodeGen/SelectionDAG.h"
#include "llvm/CodeGen/SelectionDAGNodes.h"
#include "llvm/CodeGen/TargetLowering.h"
#include <deque>

namespace llvm {
class LCCSubtarget;
namespace LCCISD {
enum NodeType : unsigned {
  FIRST_NUMBER = ISD::BUILTIN_OP_END,
  RET_FLAG,
  CALL,
  Wrapper,
  Hi,
  Lo
};
} // end namespace LCCISD

class LCCTargetLowering : public TargetLowering {
  const LCCSubtarget &Subtarget;

public:
  explicit LCCTargetLowering(const TargetMachine &TM, const LCCSubtarget &STI);

  const char *getTargetNodeName(unsigned Opcode) const override;

  /// get the ISD::SETCC result ValueType
  EVT getSetCCResultType(const DataLayout &DL, LLVMContext &Context,
                         EVT VT) const override;

  SDValue LowerOperation(SDValue Op, SelectionDAG &DAG) const override;

  SDValue PerformDAGCombine(SDNode *N, DAGCombinerInfo &DCI) const override;

  SDValue passArgOnStack(SDValue StackPtr, unsigned Offset, SDValue Chain,
                         SDValue Arg, const SDLoc &DL, bool IsTailCall,
                         SelectionDAG &DAG) const;

  SDValue LowerFormalArguments(SDValue Chain, CallingConv::ID CallConv,
                               bool IsVarArg,
                               const SmallVectorImpl<ISD::InputArg> &Ins,
                               const SDLoc &DL, SelectionDAG &DAG,
                               SmallVectorImpl<SDValue> &InVals) const override;
  bool CanLowerReturn(CallingConv::ID CallConv, MachineFunction &MF,
                      bool IsVarArg,
                      const SmallVectorImpl<ISD::OutputArg> &Outs,
                      LLVMContext &Context) const override;
  SDValue LowerReturn(SDValue Chain, CallingConv::ID CallConv, bool IsVarArg,
                      const SmallVectorImpl<ISD::OutputArg> &Outs,
                      const SmallVectorImpl<SDValue> &OutVals, const SDLoc &DL,
                      SelectionDAG &DAG) const override;

  SDValue LowerCall(CallLoweringInfo &CLI,
                    SmallVectorImpl<SDValue> &InVals) const override;

  SDValue LowerCallResult(SDValue Chain, SDValue InFlag,
                          CallingConv::ID CallConv, bool isVarArg,
                          const SmallVectorImpl<ISD::InputArg> &Ins,
                          const SDLoc &dl, SelectionDAG &DAG,
                          SmallVectorImpl<SDValue> &InVals,
                          const SDNode *CallNode, const Type *RetTy) const;

protected:
  /// ByValArgInfo - Byval argument information.
  struct ByValArgInfo {
    unsigned FirstIdx; // Index of the first register used.
    unsigned NumRegs;  // Number of registers used for this argument.
    unsigned Address;  // Offset of the stack area used to pass this argument.

    ByValArgInfo() : FirstIdx(0), NumRegs(0), Address(0) {}
  };
  /// LCCCC - This class provides methods used to analyze formal and call
  /// arguments and inquire about calling convention information.
  class LCCCC {
  public:
    LCCCC(CallingConv::ID CCID, CCState &Info)
        : CCInfo{Info}, CallConv{CCID} {};
    void analyzeCallOperands(const SmallVectorImpl<ISD::OutputArg> &Outs,
                             bool IsVarArg, bool IsSoftFloat,
                             const SDNode *CallNode,
                             std::vector<ArgListEntry> &FuncArgs);
    void analyzeFormalArguments(const SmallVectorImpl<ISD::InputArg> &Ins,
                                bool IsSoftFloat,
                                Function::const_arg_iterator FuncArg);
    void analyzeCallResult(const SmallVectorImpl<ISD::InputArg> &Ins,
                           bool IsSoftFloat, const SDNode *CallNode,
                           const Type *RetTy) const;

    void analyzeReturn(const SmallVectorImpl<ISD::OutputArg> &Outs,
                       bool IsSoftFloat, const Type *RetTy) const;

    const CCState &getCCInfo() const { return CCInfo; }

    /// hasByValArg - Returns true if function has byval arguments.
    bool hasByValArg() const { return !ByValArgs.empty(); }
    /// regSize - Size (in number of bits) of integer registers.
    unsigned regSize() const { return 4; }
    /// numIntArgRegs - Number of integer registers available for calls.
    unsigned numIntArgRegs() const;
    /// Return pointer to array of integer argument registers.
    const ArrayRef<MCPhysReg> intArgRegs() const;
    typedef SmallVectorImpl<ByValArgInfo>::const_iterator byval_iterator;
    byval_iterator byval_begin() const { return ByValArgs.begin(); }
    byval_iterator byval_end() const { return ByValArgs.end(); }

  private:
    void handleByValArg(unsigned ValNo, MVT ValVT, MVT LocVT,
                        CCValAssign::LocInfo LocInfo, ISD::ArgFlagsTy ArgFlags);

    /// useRegsForByval - Returns true if the calling convention allows the
    /// use of registers to pass byval arguments.
    bool useRegsForByval() const { return CallConv != CallingConv::Fast; }

    /// Return the function that analyzes fixed argument list functions.
    llvm::CCAssignFn *fixedArgFn() const;
    /// Return the function that analyzes variable argument list functions.
    llvm::CCAssignFn *varArgFn() const;
    void allocateRegs(ByValArgInfo &ByVal, unsigned ByValSize, unsigned Align);

    /// Return the type of the register which is used to pass an argument or
    /// return a value. This function returns f64 if the argument is an i64
    /// value which has been generated as a result of softening an f128 value.
    /// Otherwise, it just returns VT.
    MVT getRegVT(MVT VT, bool IsSoftFloat) const;

    template <typename Ty>
    void analyzeReturn(const SmallVectorImpl<Ty> &RetVals, bool IsSoftFloat,
                       const SDNode *CallNode, const Type *RetTy) const;

    CCState &CCInfo;
    CallingConv::ID CallConv;
    SmallVector<ByValArgInfo, 2> ByValArgs;
  };

public:
  // Create a TargetGlobalAddress node.
  SDValue getTargetNode(GlobalAddressSDNode *N, EVT Ty, SelectionDAG &DAG,
                        unsigned Flag) const {
    return DAG.getTargetGlobalAddress(N->getGlobal(), SDLoc(N), Ty, 0, Flag);
  }

  // Create a TargetExternalSymbol node.
  SDValue getTargetNode(ExternalSymbolSDNode *N, EVT Ty, SelectionDAG &DAG,
                        unsigned Flag) const {
    return DAG.getTargetExternalSymbol(N->getSymbol(), Ty, Flag);
  }
  // Create a TargetBlockAddress node.
  SDValue getTargetNode(BlockAddressSDNode *N, EVT Ty, SelectionDAG &DAG,
                        unsigned Flag) const {
    return DAG.getTargetBlockAddress(N->getBlockAddress(), Ty, 0, Flag);
  }

  // Create a TargetJumpTable node.
  SDValue getTargetNode(JumpTableSDNode *N, EVT Ty, SelectionDAG &DAG,
                        unsigned Flag) const {
    return DAG.getTargetJumpTable(N->getIndex(), Ty, Flag);
  }

  // This method creates the following nodes, which are necessary for
  // computing a local symbol's address:
  // (add (load (wrapper $gp, %got(sym)), %lo(sym))
  template <class NodeTy>
  SDValue getAddrLocal(NodeTy *N, EVT Ty, SelectionDAG &DAG) const {
    SDLoc DL(N);
    unsigned GOTFlag = LCC::MO_GOT;
    SDValue GOT = DAG.getNode(LCCISD::Wrapper, DL, Ty, getGlobalReg(DAG, Ty),
                              getTargetNode(N, Ty, DAG, GOTFlag));
    SDValue Load =
        DAG.getLoad(Ty, DL, DAG.getEntryNode(), GOT,
                    MachinePointerInfo::getGOT(DAG.getMachineFunction()));
    unsigned LoFlag = LCC::MO_ABS_LO;
    SDValue Lo =
        DAG.getNode(LCCISD::Lo, DL, Ty, getTargetNode(N, Ty, DAG, LoFlag));
    return DAG.getNode(ISD::ADD, DL, Ty, Load, Lo);
  }

  // This method creates the following nodes, which are necessary for
  // computing a global symbol's address:
  // (load (wrapper $gp, %got(sym)))
  template <class NodeTy>
  SDValue getAddrGlobal(NodeTy *N, EVT Ty, SelectionDAG &DAG, unsigned Flag,
                        SDValue Chain,
                        const MachinePointerInfo &PtrInfo) const {
    SDLoc DL(N);
    SDValue Tgt = DAG.getNode(LCCISD::Wrapper, DL, Ty, getGlobalReg(DAG, Ty),
                              getTargetNode(N, Ty, DAG, Flag));
    return DAG.getLoad(Ty, DL, Chain, Tgt, PtrInfo);
  }

  /// copyByValArg - Copy argument registers which were used to pass a byval
  /// argument to the stack. Create a stack frame object for the byval
  /// argument.
  void copyByValRegs(SDValue Chain, const SDLoc &DL,
                     std::vector<SDValue> &OutChains, SelectionDAG &DAG,
                     const ISD::ArgFlagsTy &Flags,
                     SmallVectorImpl<SDValue> &InVals, const Argument *FuncArg,
                     const LCCCC &CC, const ByValArgInfo &ByVal) const;
  /// passByValArg - Pass a byval argument in registers or on stack.
  void passByValArg(SDValue Chain, const SDLoc &DL,
                    std::deque<std::pair<unsigned, SDValue>> &ReogsToPass,
                    SmallVectorImpl<SDValue> &MemOpChains, SDValue StackPtr,
                    MachineFrameInfo &MFI, SelectionDAG &DAG, SDValue Arg,
                    const LCCCC &CC, const ByValArgInfo &ByVal,
                    const ISD::ArgFlagsTy &Flags, bool isLittle) const;

  /// This function fills Ops, which is the list of operands that will later
  /// be used when a function call node is created. It also generates
  /// copyToReg nodes to set up argument registers.
  void getOpndList(SmallVectorImpl<SDValue> &Ops,
                   std::deque<std::pair<unsigned, SDValue>> &RegsToPass,
                   bool IsPICCall, bool GlobalOrExternal, bool InternalLinkage,
                   CallLoweringInfo &CLI, SDValue Callee, SDValue Chain) const;

  SDValue getGlobalReg(SelectionDAG &DAG, EVT Ty) const;
};

} // end namespace llvm

#endif
