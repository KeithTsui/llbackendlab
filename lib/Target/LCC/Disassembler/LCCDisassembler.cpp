#include "MCTargetDesc/LCCMCTargetDesc.h"
#include "TargetInfo/LCCTargetInfo.h"
#include "llvm/MC/MCDisassembler/MCDisassembler.h"
#include "llvm/MC/MCFixedLenDisassembler.h"
#include "llvm/MC/MCInst.h"
#include "llvm/MC/MCSubtargetInfo.h"
#include "llvm/Support/MathExtras.h"
#include "llvm/Support/TargetRegistry.h"
#include <cassert>
#include <cstdint>

#define DEBUG_TYPE "lcc-disassembler"

using namespace llvm;

using DecodeStatus = MCDisassembler::DecodeStatus;

namespace {

class LCCDisassembler : public MCDisassembler {
public:
  LCCDisassembler(const MCSubtargetInfo &STI, MCContext &Ctx)
      : MCDisassembler(STI, Ctx) {}
  ~LCCDisassembler() override = default;

  DecodeStatus getInstruction(MCInst &instr, uint64_t &Size,
                              ArrayRef<uint8_t> Bytes, uint64_t Address,
                              raw_ostream &CStream) const override;
};

} // end anonymous namespace

static MCDisassembler *createLCCDisassembler(const Target &T,
                                             const MCSubtargetInfo &STI,
                                             MCContext &Ctx) {
  return new LCCDisassembler(STI, Ctx);
}

extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeLCCDisassembler() {
  // Register the disassembler.
  TargetRegistry::RegisterMCDisassembler(getTheLCCTarget(),
                                         createLCCDisassembler);
}

static const uint16_t GPRDecoderTable[] = {
    LCC::R0, LCC::R1, LCC::R2, LCC::R3, LCC::R4, LCC::R5, LCC::R6, LCC::R7,
};

static DecodeStatus DecodeGPRRegisterClass(MCInst &Inst, uint64_t RegNo,
                                           uint64_t Address,
                                           const void *Decoder) {
  if (RegNo > 31)
    return MCDisassembler::Fail;

  unsigned Register = GPRDecoderTable[RegNo];
  Inst.addOperand(MCOperand::createReg(Register));
  return MCDisassembler::Success;
}

template <unsigned N>
static DecodeStatus decodeUImmOperand(MCInst &Inst, uint64_t Imm) {
  if (!isUInt<N>(Imm))
    return MCDisassembler::Fail;
  Inst.addOperand(MCOperand::createImm(Imm));
  return MCDisassembler::Success;
}
static DecodeStatus decodeU4ImmOperand(MCInst &Inst, uint64_t Imm,
                                       uint64_t Address, const void *Decoder) {
  return decodeUImmOperand<4>(Inst, Imm);
}


template <unsigned N>
static DecodeStatus decodeImmOperand(MCInst &Inst, int64_t Imm) {
  if (!isInt<N>(Imm))
    return MCDisassembler::Fail;
  Inst.addOperand(MCOperand::createImm(Imm));
  return MCDisassembler::Success;
}

static DecodeStatus decodeS6ImmOperand(MCInst &Inst, int64_t Imm,
                                       uint64_t Address, const void *Decoder) {
  return decodeImmOperand<6>(Inst, Imm);
}

static DecodeStatus decodeS5ImmOperand(MCInst &Inst, int64_t Imm,
                                       uint64_t Address, const void *Decoder) {
  return decodeImmOperand<5>(Inst, Imm);
}

static DecodeStatus decodeS9ImmOperand(MCInst &Inst, int64_t Imm,
                                       uint64_t Address, const void *Decoder) {
  return decodeImmOperand<9>(Inst, Imm);
}


static DecodeStatus decodePC9BranchOperand(MCInst &Inst, uint64_t Imm,
                                           uint64_t Address,
                                           const void *Decoder) {
  if (!isUInt<26>(Imm))
    return MCDisassembler::Fail;
  Inst.addOperand(MCOperand::createImm(SignExtend64<26>(Imm)));
  return MCDisassembler::Success;
}


#include "LCCGenDisassemblerTables.inc"

DecodeStatus LCCDisassembler::getInstruction(MCInst &MI, uint64_t &Size,
                                             ArrayRef<uint8_t> Bytes,
                                             uint64_t Address,
                                             raw_ostream &CS) const {
  // Instruction size is always 32 bit.
  if (Bytes.size() < 4) {
    Size = 0;
    return MCDisassembler::Fail;
  }
  Size = 4;

  // Construct the instruction.
  uint32_t Inst = 0;
  for (uint32_t I = 0; I < Size; ++I)
    Inst = (Inst << 8) | Bytes[I];

  return decodeInstruction(DecoderTableLCC16, MI, Inst, Address, this, STI);
}
