#include "TargetInfo/LCCTargetInfo.h"
#include "llvm/Support/TargetRegistry.h"
using namespace llvm;
Target &llvm::getTheLCCTarget() {
  static Target TheLCCTarget;
  return TheLCCTarget;
}
extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeLCCTargetInfo() {
  RegisterTarget<Triple::lcc, /*HasJIT=*/false> X(getTheLCCTarget(), "lcc",
                                                   "LCC", "LCC");
}
