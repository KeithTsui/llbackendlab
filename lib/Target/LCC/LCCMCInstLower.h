#ifndef LLVM_LIB_TARGET_LCC_LCCMCINSTLOWER_H
#define LLVM_LIB_TARGET_LCC_LCCMCINSTLOWER_H
#include "llvm/CodeGen/MachineOperand.h"
#include "llvm/MC/MCExpr.h"
namespace llvm {
class AsmPrinter;
class MCInst;
class MCOperand;
class MachineInstr;
class MachineOperand;
class Mangler;

class LLVM_LIBRARY_VISIBILITY LCCMCInstLower {
  typedef MachineOperand::MachineOperandType MachineOperandType;
  MCContext &Ctx;
  AsmPrinter &Printer;

public:
  LCCMCInstLower(MCContext &Ctx, AsmPrinter &Printer);

  // Lower MachineInstr MI to MCInst OutMI.
  void lower(const MachineInstr *MI, MCInst &OutMI) const;

  // Return an MCOperand for MO.
  MCOperand lowerOperand(const MachineOperand &MO, unsigned Offset = 0) const;

  MCOperand LowerSymbolOperand(const MachineOperand &MO,
                               MachineOperandType MOTy,
                               unsigned Offset = 0) const;

  // Return an MCExpr for symbolic operand MO with variant kind Kind.
  const MCExpr *getExpr(const MachineOperand &MO,
                        MCSymbolRefExpr::VariantKind Kind) const;
};
} // end namespace llvm

#endif
