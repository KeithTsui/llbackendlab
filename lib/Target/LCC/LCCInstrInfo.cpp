#include "LCCInstrInfo.h"
#include "LCCAnalyzeImmediate.h"
#include "MCTargetDesc/LCCMCTargetDesc.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/MathExtras.h"
#include <MacTypes.h>
#include <cassert>

using namespace llvm;
#define GET_INSTRINFO_CTOR_DTOR
#define GET_INSTRMAP_INFO
#include "LCCGenInstrInfo.inc"

#define DEBUG_TYPE "lcc-ii"
namespace llvm {

// Pin the vtable to this file.
void LCCInstrInfo::anchor() {}

LCCInstrInfo::LCCInstrInfo(LCCSubtarget &STI)
    : LCCGenInstrInfo(LCC::ADJCALLSTACKDOWN, LCC::ADJCALLSTACKUP), RI(),
      STI(STI) {}

MachineMemOperand *
LCCInstrInfo::GetMemOperand(MachineBasicBlock &MBB, int FI,
                            MachineMemOperand::Flags Flags) const {

  MachineFunction &MF = *MBB.getParent();
  MachineFrameInfo &MFI = MF.getFrameInfo();
  return MF.getMachineMemOperand(MachinePointerInfo::getFixedStack(MF, FI),
                                 Flags, MFI.getObjectSize(FI),
                                 MFI.getObjectAlign(FI));
}

void LCCInstrInfo::storeRegToStackSlot(MachineBasicBlock &MBB,
                                       MachineBasicBlock::iterator MBBI,
                                       Register SrcReg, bool isKill,
                                       int FrameIndex,
                                       const TargetRegisterClass *RC,
                                       const TargetRegisterInfo *TRI) const {
  DebugLoc DL;
  MachineMemOperand *MMO =
      GetMemOperand(MBB, FrameIndex, MachineMemOperand::MOStore);
  BuildMI(MBB, MBBI, DL, get(LCC::STm))
      .addReg(SrcReg, getKillRegState(isKill))
      .addFrameIndex(FrameIndex)
      .addImm(0)
      .addMemOperand(MMO);
}

void LCCInstrInfo::loadRegFromStackSlot(MachineBasicBlock &MBB,
                                        MachineBasicBlock::iterator MBBI,
                                        Register DestReg, int FrameIndex,
                                        const TargetRegisterClass *RC,
                                        const TargetRegisterInfo *TRI) const {
  DebugLoc DL = MBBI != MBB.end() ? MBBI->getDebugLoc() : DebugLoc();
  MachineMemOperand *MMO =
      GetMemOperand(MBB, FrameIndex, MachineMemOperand::MOLoad);
  // fix me with load.
  BuildMI(MBB, MBBI, DL, get(LCC::LDm), DestReg)
      .addFrameIndex(FrameIndex)
      .addImm(0)
      .addMemOperand(MMO);
}

/// Adjust SP by Amount bytes.
void LCCInstrInfo::adjustStackPtr(unsigned SP, int64_t Amount,
                                  MachineBasicBlock &MBB,
                                  MachineBasicBlock::iterator I) const {
  DebugLoc DL = I != MBB.end() ? I->getDebugLoc() : DebugLoc();
  if (isInt<5>(Amount)) { // fit in 5-bits
    // addiu sp, sp, amount
    BuildMI(MBB, I, DL, get(LCC::ADDri), SP).addReg(SP).addImm(Amount);
  } else if (isInt<9>(Amount)) { // fit in 9 bits
    // MVI $0, imm9
    // ADDrr SP, SP, $0
    unsigned Reg = LCC::R0;
    BuildMI(MBB, I, DL, get(LCC::MVI), Reg).addImm(Amount);
    BuildMI(MBB, I, DL, get(LCC::ADDrr), SP)
        .addReg(SP)
        .addReg(Reg, RegState::Kill);
  } else { // should be 16-bits, because
    assert(isInt<16>(Amount) &&
           "Stack size should be in 16-btis, because it's 16-bits machine that "
           "the size of addressable space is 16-bits.");
    // MVI $0, HI(imm16)
    // SLL $0, 8
    // MVI $1, LO(imm16)
    // OR $0, $1
    // ADDrr SP, SP, $0
    SInt16 int16 = static_cast<SInt16>(Amount);
    unsigned Reg = LCC::R0;
    unsigned Reg1 = LCC::R1;
    BuildMI(MBB, I, DL, get(LCC::MVI), Reg).addImm(int16 >> 8);
    BuildMI(MBB, I, DL, get(LCC::SLL), Reg).addImm(8);
    BuildMI(MBB, I, DL, get(LCC::MVI), Reg1).addImm(int16 & 0xFF);
    BuildMI(MBB, I, DL, get(LCC::OR), Reg).addReg(Reg1);
    BuildMI(MBB, I, DL, get(LCC::ADDrr), SP)
        .addReg(SP)
        .addReg(Reg, RegState::Kill);
  }
}

/// This function generates the sequence of instructions needed to get the
/// result of adding register REG and immediate IMM.
unsigned LCCInstrInfo::loadImmediate(int64_t Imm, MachineBasicBlock &MBB,
                                     MachineBasicBlock::iterator II,
                                     const DebugLoc &DL,
                                     unsigned *NewImm) const {
  llvm_unreachable("not implemented yet");
  return 0;
}

void LCCInstrInfo::copyPhysReg(MachineBasicBlock &MBB,
                               MachineBasicBlock::iterator I,
                               const DebugLoc &DL, MCRegister DestReg,
                               MCRegister SrcReg, bool KillSrc) const {
  unsigned Opc = LCC::MVR;
  MachineInstrBuilder MIB = BuildMI(MBB, I, DL, get(Opc));
  if (DestReg)
    MIB.addReg(DestReg, RegState::Define);
  if (SrcReg)
    MIB.addReg(SrcReg, getKillRegState(KillSrc));
}
} // namespace llvm
