//===-- LCCFunctionInfo.cpp -*- C++ -*-===//
#include "LCCFunctionInfo.h"
#include "LCCInstrInfo.h"
#include "LCCSubtarget.h"
#include "MCTargetDesc/LCCMCTargetDesc.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/IR/Function.h"
using namespace llvm;
static bool FixGlobalBaseReg;
LCCFunctionInfo::~LCCFunctionInfo() {}
bool LCCFunctionInfo::globalBaseRegFixed() const { return FixGlobalBaseReg; }

bool LCCFunctionInfo::globalBaseRegSet() const { return GlobalBaseReg; }

unsigned LCCFunctionInfo::getGlobalBaseReg() {
  return GlobalBaseReg = LCC::R0;
}
void LCCFunctionInfo::createEhDataRegsFI() {
  const TargetRegisterInfo &TRI = *MF.getSubtarget().getRegisterInfo();
  for (int I = 0; I < 2; ++I) {
    const TargetRegisterClass &RC = LCC::GPRRegClass;
    EhDataRegFI[I] = MF.getFrameInfo().CreateStackObject(
        TRI.getSpillSize(RC), TRI.getSpillAlign(RC), false);
  }
}
MachinePointerInfo LCCFunctionInfo::callPtrInfo(const char *ES) {
  return MachinePointerInfo(MF.getPSVManager().getExternalSymbolCallEntry(ES));
}
MachinePointerInfo LCCFunctionInfo::callPtrInfo(const GlobalValue *GV) {
  return MachinePointerInfo(MF.getPSVManager().getGlobalValueCallEntry(GV));
}
void LCCFunctionInfo::anchor() {}
