// These are target-independent nodes, but have target-specific formats.
def SDT_CallSeqStart : SDCallSeqStart<[SDTCisVT<0, i16>, SDTCisVT<1, i16>]>;
def SDT_CallSeqEnd   : SDCallSeqEnd<[SDTCisVT<0, i16>, SDTCisVT<1, i16>]>;
def SDT_Call         : SDTypeProfile<0, -1, [SDTCisPtrTy<0>]>;


def callseq_start : SDNode<"ISD::CALLSEQ_START", SDT_CallSeqStart,
                           [SDNPHasChain, SDNPOutGlue]>;
def callseq_end   : SDNode<"ISD::CALLSEQ_END", SDT_CallSeqEnd,
                           [SDNPHasChain, SDNPOptInGlue, SDNPOutGlue]>;
def uimm16      : Operand<i16> {
  let PrintMethod = "printImmOperand";
}
let Defs = [SP], Uses = [SP] in {
def ADJCALLSTACKDOWN : Pseudo<(outs), (ins uimm16:$amt1, uimm16:$amt2),
                              [(callseq_start timm:$amt1, timm:$amt2)]>;
def ADJCALLSTACKUP   : Pseudo<(outs), (ins uimm16:$amt1, uimm16:$amt2),
                               [(callseq_end timm:$amt1, timm:$amt2)]>;
}


// Selection DAG nodes.
def call             : SDNode<"LCCISD::CALL", SDT_Call,
                              [SDNPHasChain, SDNPOptInGlue, SDNPOutGlue,
                              SDNPVariadic]>;
def retflag          : SDNode<"LCCISD::RET_FLAG", SDTNone,
                              [SDNPHasChain, SDNPOptInGlue, SDNPVariadic]>;
                              
def ToTC : SDNodeXForm<imm, [{
  int64_t Value = N->getSExtValue();
  return CurDAG->getTargetConstant(Value, SDLoc(N), MVT::i16);
}]>;

class ImmediateAsmOperand<string name> : AsmOperandClass {
  let Name = name;
  let RenderMethod = "addImmOperands";
}

def S5Imm : ImmediateAsmOperand<"S5Imm">;
def S6Imm : ImmediateAsmOperand<"S6Imm">;
def S9Imm : ImmediateAsmOperand<"S9Imm">;

// immediate operand
class ImmediateOp<ValueType vt, string asmop> : Operand<vt> {
  let PrintMethod = "printImmOperand";
  // static DecodeStatus #DecoderMethod(MCInst &Inst, int64_t Imm,
  //                                    uint64_t Address, const void *Decoder)
  let DecoderMethod = "decode"#asmop#"Operand";
  let ParserMatchClass = !cast<AsmOperandClass>(asmop);
  let OperandType = "OPERAND_IMMEDIATE";
}

class ImmOpWithPattern<ValueType vt, string asmop, code pred, SDNodeXForm xform,
      SDNode ImmNode = imm> :
  ImmediateOp<vt, asmop>, PatLeaf<(vt ImmNode), pred, xform>;

multiclass Immediate<ValueType vt, code pred, SDNodeXForm xform, string asmop> {
  // llvm ir constant
  def "" : ImmOpWithPattern<vt, asmop, pred, xform>;
  // target constant
  def _timm : ImmOpWithPattern<vt, asmop, pred, xform, timm>;
}

defm imm16InL5 : Immediate<i16, [{
  int64_t Value = N->getSExtValue();
  return (-16 <= Value && Value <= 16);
}], ToTC, "S5Imm">;

defm imm16InL6 : Immediate<i16, [{
  int64_t Value = N->getSExtValue();
  return (-32 <= Value && Value <= 31);
}], ToTC, "S6Imm">;

defm imm16InL9 : Immediate<i16, [{
  int64_t Value = N->getSExtValue();
  return (-256 <= Value && Value <= 255);
}], ToTC, "S9Imm">;

def LCCMemAsmOperand : AsmOperandClass {
  let Name = "Mem";
  // OperandMatchResultTy XXXMCTargetAsmParser::parseMemOperand(OperandVector &);
  let ParserMethod = "parseMemOperand";
  //void XXXMCParsedAsmOperand::addMemOperand(MCInst &Inst, unsigned N) const
  let RenderMethod = "addMemOperand";
}

// Address operand MachineInstruction/MCInst Operand
def frameSlot : Operand<iPTR> {
  // void XXXInstPrinter::#PrintMethod(const MCInst *MI, unsigned opNo,
  //                                       const MCSubtargetInfo &STI,
  //                                       raw_ostream &O);
  let PrintMethod = "printMemOperand";
  // unsigned XXXMCCodeEmitter::#EncoderMethod(const MCInst &MI, unsigned OpNo,
  //                         SmallVectorImpl<MCFixup> &Fixups,
  //                         const MCSubtargetInfo &STI) const
  let EncoderMethod = "getMemEncoding";
  
  let MIOperandInfo = (ops GPROpnd, imm16InL6);
  let ParserMatchClass = LCCMemAsmOperand;
}

def frameObject : 
  ComplexPattern<iPTR, 2, "SelectFrameObject", [frameindex], [SDNPWantParent]>;

def LDm : F_LSR<0b0110,
               (outs GPROpnd:$r), (ins frameSlot:$obj),
               !strconcat("ldr", " $r, $obj"),
               [(set GPROpnd:$r, (load frameObject:$obj))]
               >;

def STm: F_LSR<0b0111,
               (outs), (ins  GPROpnd:$r, frameSlot:$obj),
               !strconcat("str", " $r, $obj"),
               [(store GPROpnd:$r, frameObject:$obj)]
               >;

let isReturn = 1, isTerminator = 1, isBarrier = 1 in {
  def RET_LR: F_RET<(outs),(ins), "ret", [(retflag)]>;
}

def MVR: F_MD<0b1100,
             (outs GPROpnd:$rd), (ins  GPROpnd:$rs1),
             !strconcat("mvr", " $rd, $rs1")>;

def MVI: F_MVI<
          (outs GPROpnd:$dr),
          (ins imm16InL9:$imm9),
          !strconcat("mvi", " $dr, $imm9")>;

// Instructions
multiclass F_ASIR<bits<4> FuncI, string OpcStr, SDNode OpNode> {
  def ri  : F_ASI<FuncI,
                  (outs GPROpnd:$rd),
                  (ins GPROpnd:$rs1, imm16InL5:$imm5),
                  !strconcat(OpcStr, " $rd, $rs1, $imm5"),
                  [(set i16:$rd, (OpNode GPROpnd:$rs1, imm16InL5:$imm5))]
                  >;

  def rr  : F_ASR<FuncI,
                  (outs GPROpnd:$rd),
                  (ins GPROpnd:$rs1, GPROpnd:$rs2),
                  !strconcat(OpcStr, " $rd, $rs1, $rs2"),
                  [(set i16:$rd, (OpNode GPROpnd:$rs1, GPROpnd:$rs2))]
                  >;
}
let isCommutable = 1 in {
  defm AND: F_ASIR<0b0101, "and", and>;
  defm ADD: F_ASIR<0b0001, "add", add>;
}
defm SUB: F_ASIR<0b1011, "sub", sub>;

def U4Imm : ImmediateAsmOperand<"U4Imm">;
defm imm16L4U : Immediate<i16, [{
  return (N->getZExtValue() & ~0x000000000000000fULL) == 0;
}], ToTC, "U4Imm">;
class F_SHFT<bits<3> func, string OpcStr, SDNode OpNode>
  : F_SHF<func,
          (outs GPROpnd:$dr),
          (ins GPROpnd:$rs1, imm16L4U:$ct),
          !strconcat(OpcStr, " $rs1, $ct"),
          [(set GPROpnd:$dr, (OpNode GPROpnd:$rs1, imm16L4U:$ct))]>{
  let Constraints = "$dr = $rs1";
}
def SRL: F_SHFT<0b010, "srl", srl>;
def SRA: F_SHFT<0b011, "sra", sra>;
def SLL: F_SHFT<0b100, "sll", shl>;
def ROL: F_SHFT<0b101, "rol", rotl>;
def ROR: F_SHFT<0b110, "ror", rotr>;

def PUSH: F_PP<0,
             (outs),
             (ins GPROpnd:$r),
             "push $r">;

def POP: F_PP<1,
             (outs GPROpnd:$r),
             (ins),
             "pop $r">;

class F_MDT<bits<4> func, string OpcStr, SDNode Op>
  : F_MD<func,
          (outs GPROpnd:$dr),
          (ins GPROpnd:$rs1, GPROpnd:$rs2),
          !strconcat(OpcStr, " $rs1, $rs2"),
          [(set GPROpnd:$dr, (Op GPROpnd:$rs1, GPROpnd:$rs2))]> {
  let Constraints = "$dr = $rs1";
}

let isCommutable = 1 in {
  def MUL: F_MDT<0b0111, "mul", mul>;
  def OR: F_MDT<0b1010, "or", or>;
  def XOR: F_MDT<0b1011, "xor", xor>;
}

def DIV: F_MDT<0b1000, "div", sdiv>;
def REM: F_MDT<0b1001, "rem", srem>;


def NOT: F_NOT<0b0000,
          (outs GPROpnd:$dr),
          (ins GPROpnd:$rs1),
          !strconcat("not", " $dr, $rs1"),
          [(set GPROpnd:$dr, (not GPROpnd:$rs1))]>;

def CMPri : F_CMPI<(outs SR:$r0), (ins GPROpnd:$rs1, imm16InL5:$imm5),
                   "cmp $rs1, $imm5">;
def CMPrr : F_CMPR<(outs SR:$r0), (ins GPROpnd:$rs1, GPROpnd:$rs2),
                   "cmp $rs1, $rs2">;

// Constructs an operand for a PC-relative address with address type VT.
// ASMOP is the associated asm operand.
class PCRelOperand<ValueType vt, AsmOperandClass asmop> : Operand<vt> {
  let PrintMethod = "printPCRelOperand";
  let ParserMatchClass = asmop;
  let OperandType = "OPERAND_PCREL";
}
// Constructs an asm operand for a PC-relative address.  SIZE says how
// many bits there are.
class PCRelAsmOperand<string size> : ImmediateAsmOperand<"PCRel"#size> {
  let PredicateMethod = "isImm";
  let ParserMethod = "parsePCRel"#size;
}

def PCRel11 : PCRelAsmOperand<"11">;
def brtarget11 : PCRelOperand<OtherVT, PCRel11> {
  let EncoderMethod = "getPC11Encoding";
  let DecoderMethod = "decodePC11BranchOperand";
}

def PCRel9 : PCRelAsmOperand<"9">;
def brtarget9 : PCRelOperand<OtherVT, PCRel9> {
  let EncoderMethod = "getPC9Encoding";
  let DecoderMethod = "decodePC9BranchOperand";
}

def calltarget  : PCRelOperand<iPTR, PCRel11> {
  let EncoderMethod = "getPC11Encoding";
}
let isCall = 1, isTerminator = 1, isBarrier = 1 in {
 def JSR : F_JSR<(outs), (ins calltarget:$d11),"jsr $d11">;
 def JSRR : F_JSRR<(outs), (ins GPROpnd:$baser, imm16InL6:$imm6, variable_ops),
                   "jsrr $baser, $imm6">;
}

def LCCWrapper    : SDNode<"LCCISD::Wrapper", SDTIntBinOp>;
class WrapperPat<SDNode node, Instruction ORiOp, RegisterClass RC>:
      Pat<(LCCWrapper RC:$gp, node:$in),
              (ORiOp RC:$gp, node:$in)>;
def : WrapperPat<tglobaladdr, ADDri, GPR>;
def : WrapperPat<tjumptable, ADDri, GPR>;
def : WrapperPat<tglobaltlsaddr, ADDri, GPR>;


// Pattern for matching
// patterns for constants
def : Pat<(i16 imm16InL9:$imm9),
          (MVI imm16InL9:$imm9)>;
def : Pat<(call (i16 tglobaladdr:$dst)),
          (JSR tglobaladdr:$dst)>;
def : Pat<(call (i16 texternalsym:$dst)),
          (JSR texternalsym:$dst)>;


def jmptarget    : Operand<OtherVT> {
  let EncoderMethod = "getJumpTargetOpValue";
  let OperandType = "OPERAND_PCREL";
}

// unconditional branch
let isBranch = 1, isTerminator = 1, isBarrier = 1, hasDelaySlot = 0 in {
  def BR : F_BRANCH<0b111, (outs), (ins jmptarget:$addr), "br $addr",
                    [(br bb:$addr)]>;
}
let isBranch = 1, isTerminator = 1, hasDelaySlot = 0 in {
  def BRNE : F_BRANCH<0b001, (outs), (ins SR:$ra, brtarget9:$addr), "brne $addr">;
  def BRE  : F_BRANCH<0b000, (outs), (ins SR:$ra, brtarget9:$addr), "bre $addr">;
  def BRN  : F_BRANCH<0b010, (outs), (ins SR:$ra, brtarget9:$addr), "brn $addr">;
  def BRP  : F_BRANCH<0b011, (outs), (ins SR:$ra, brtarget9:$addr), "brp $addr">;
  def BRLT : F_BRANCH<0b100, (outs), (ins SR:$ra, brtarget9:$addr), "brlt $addr">;
  def BRGT : F_BRANCH<0b101, (outs), (ins SR:$ra, brtarget9:$addr), "brgt $addr">;
  def BRC  : F_BRANCH<0b110, (outs), (ins SR:$ra, brtarget9:$addr), "brc $addr">;
}

multiclass BrcondPatsCmp<RegisterClass RC,
                         Instruction JEQOp,
                         Instruction JNEOp, 
                         Instruction JLTOp,
                         Instruction JGTOp,
                         Instruction CMPOp> {
def : Pat<(brcond (i16 (seteq RC:$lhs, RC:$rhs)), bb:$dst),
          (JEQOp (CMPOp RC:$lhs, RC:$rhs), bb:$dst)>;
def : Pat<(brcond (i16 (setne RC:$lhs, RC:$rhs)), bb:$dst),
          (JNEOp (CMPOp RC:$lhs, RC:$rhs), bb:$dst)>;
def : Pat<(brcond (i16 (setlt RC:$lhs, RC:$rhs)), bb:$dst),
          (JLTOp (CMPOp RC:$lhs, RC:$rhs), bb:$dst)>;
def : Pat<(brcond (i16 (setgt RC:$lhs, RC:$rhs)), bb:$dst),
          (JGTOp (CMPOp RC:$lhs, RC:$rhs), bb:$dst)>;
// if $lhs <= $rhs then goto $dst === if $rhs > $lhs then goto $dst
def : Pat<(brcond (i16 (setle RC:$lhs, RC:$rhs)), bb:$dst),
          (JGTOp (CMPOp RC:$rhs, RC:$lhs), bb:$dst)>;
// if $lhs >= $rhs then goto $dst === if $rhs < $lhs then goto $dst          
def : Pat<(brcond (i16 (setge RC:$lhs, RC:$rhs)), bb:$dst),
          (JLTOp (CMPOp RC:$rhs, RC:$lhs), bb:$dst)>;
def : Pat<(brcond RC:$cond, bb:$dst),
          (JNEOp (CMPri RC:$cond, 0), bb:$dst)>;
}

defm : BrcondPatsCmp<GPROpnd, BRE, BRNE, BRLT, BRGT, CMPrr>;
