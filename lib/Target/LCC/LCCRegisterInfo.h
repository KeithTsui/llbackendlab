#ifndef LLVM_LIB_TARGET_LCC_LCCREGISTERINFO_H
#define LLVM_LIB_TARGET_LCC_LCCREGISTERINFO_H
#define GET_REGINFO_HEADER
#include "LCCGenRegisterInfo.inc"
namespace llvm {
struct LCCRegisterInfo : public LCCGenRegisterInfo {
  LCCRegisterInfo();

  /// Code Generation virtual methods...
  const MCPhysReg *getCalleeSavedRegs(const MachineFunction *MF) const override;
  const uint32_t *getCallPreservedMask(const MachineFunction &MF,
                                       CallingConv::ID) const override;

  BitVector getReservedRegs(const MachineFunction &MF) const override;

  void eliminateFrameIndex(MachineBasicBlock::iterator II, int SPAdj,
                           unsigned FIOperandNum,
                           RegScavenger *RS = nullptr) const override;

  Register getFrameRegister(const MachineFunction &MF) const override;


};

} // end namespace llvm

#endif
