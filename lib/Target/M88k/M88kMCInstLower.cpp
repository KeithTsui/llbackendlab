#include "M88kMCInstLower.h"
#include "MCTargetDesc/M88kBaseInfo.h"
#include "MCTargetDesc/M88kMCExpr.h"
#include "llvm/CodeGen/AsmPrinter.h"
#include "llvm/CodeGen/MachineInstr.h"
#include "llvm/CodeGen/MachineOperand.h"
#include "llvm/IR/Mangler.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCInst.h"
#include "llvm/MC/MCStreamer.h"

using namespace llvm;

// Return the VK_* enumeration for MachineOperand target flags Flags.
static MCSymbolRefExpr::VariantKind getVariantKind(unsigned Flags) {
  // TODO Implement
  return MCSymbolRefExpr::VK_None;
}

M88kMCInstLower::M88kMCInstLower(MCContext &Ctx, AsmPrinter &Printer)
    : Ctx(Ctx), Printer(Printer) {}

const MCExpr *
M88kMCInstLower::getExpr(const MachineOperand &MO,
                         MCSymbolRefExpr::VariantKind Kind) const {
  const MCSymbol *Symbol;
  bool HasOffset = true;
  switch (MO.getType()) {
  case MachineOperand::MO_MachineBasicBlock:
    Symbol = MO.getMBB()->getSymbol();
    HasOffset = false;
    break;

  case MachineOperand::MO_GlobalAddress:
    Symbol = Printer.getSymbol(MO.getGlobal());
    break;

  case MachineOperand::MO_ExternalSymbol:
    Symbol = Printer.GetExternalSymbolSymbol(MO.getSymbolName());
    break;

  case MachineOperand::MO_JumpTableIndex:
    Symbol = Printer.GetJTISymbol(MO.getIndex());
    HasOffset = false;
    break;

  case MachineOperand::MO_ConstantPoolIndex:
    Symbol = Printer.GetCPISymbol(MO.getIndex());
    break;

  case MachineOperand::MO_BlockAddress:
    Symbol = Printer.GetBlockAddressSymbol(MO.getBlockAddress());
    break;

  default:
    llvm_unreachable("unknown operand type");
  }
  const MCExpr *Expr = MCSymbolRefExpr::create(Symbol, Kind, Ctx);
  if (HasOffset)
    if (int64_t Offset = MO.getOffset()) {
      const MCExpr *OffsetExpr = MCConstantExpr::create(Offset, Ctx);
      Expr = MCBinaryExpr::createAdd(Expr, OffsetExpr, Ctx);
    }
  return Expr;
}

MCOperand M88kMCInstLower::lowerOperand(const MachineOperand &MO,
                                        unsigned offset) const {
  switch (MO.getType()) {
  case MachineOperand::MO_Register:
    return MCOperand::createReg(MO.getReg());
  case MachineOperand::MO_Immediate:
    return MCOperand::createImm(MO.getImm());
  case MachineOperand::MO_MachineBasicBlock:
  case MachineOperand::MO_ExternalSymbol:
  case MachineOperand::MO_JumpTableIndex:
  case MachineOperand::MO_BlockAddress:
  case MachineOperand::MO_GlobalAddress:
    return LowerSymbolOperand(MO, MO.getType(), offset);
  case MachineOperand::MO_RegisterMask:
    break;

  default: {
    llvm_unreachable("unknown operand type");
  }
  }
  return MCOperand();
}

MCOperand M88kMCInstLower::LowerSymbolOperand(const MachineOperand &MO,
                                              MachineOperandType MOTy,
                                              unsigned Offset) const {
  MCSymbolRefExpr::VariantKind Kind = MCSymbolRefExpr::VK_None;
  M88kMCExpr::M88kExprKind TargetKind = M88kMCExpr::CEK_None;
  const MCSymbol *Symbol;

  switch (MO.getTargetFlags()) {
  default:
    llvm_unreachable("Invalid target flag!");
  case M88k::MO_NO_FLAG:
    break;

    // M88k_GPREL is for llc -march=cpu0 -relocation-model=static -cpu0-islinux-
    //  format=false (global var in .sdata).
  case M88k::MO_GPREL:
    TargetKind = M88kMCExpr::CEK_GPREL;
    break;
  case M88k::MO_GOT_CALL:
    TargetKind = M88kMCExpr::CEK_GOT_CALL;
    break;
  case M88k::MO_GOT:
    TargetKind = M88kMCExpr::CEK_GOT;
    break;
    // ABS_HI and ABS_LO is for llc -march=cpu0 -relocation-model=static (global
    //  var in .data).
  case M88k::MO_ABS_HI:
    TargetKind = M88kMCExpr::CEK_ABS_HI;
    break;
  case M88k::MO_ABS_LO:
    TargetKind = M88kMCExpr::CEK_ABS_LO;
    break;
  case M88k::MO_TLSGD:
    TargetKind = M88kMCExpr::CEK_TLSGD;
    break;
  case M88k::MO_TLSLDM:
    TargetKind = M88kMCExpr::CEK_TLSLDM;
    break;
  case M88k::MO_DTP_HI:
    TargetKind = M88kMCExpr::CEK_DTP_HI;
    break;
  case M88k::MO_DTP_LO:
    TargetKind = M88kMCExpr::CEK_DTP_LO;
    break;
  case M88k::MO_GOTTPREL:
    TargetKind = M88kMCExpr::CEK_GOTTPREL;
    break;
  case M88k::MO_TP_HI:
    TargetKind = M88kMCExpr::CEK_TP_HI;
    break;
  case M88k::MO_TP_LO:
    TargetKind = M88kMCExpr::CEK_TP_LO;
    break;
  case M88k::MO_GOT_HI16:
    TargetKind = M88kMCExpr::CEK_GOT_HI16;
    break;
  case M88k::MO_GOT_LO16:
    TargetKind = M88kMCExpr::CEK_GOT_LO16;
    break;
  }

  switch (MOTy) {
  case MachineOperand::MO_GlobalAddress:
    Symbol = Printer.getSymbol(MO.getGlobal());
    Offset += MO.getOffset();
    break;

  case MachineOperand::MO_MachineBasicBlock:
    Symbol = MO.getMBB()->getSymbol();
    break;

  case MachineOperand::MO_BlockAddress:
    Symbol = Printer.GetBlockAddressSymbol(MO.getBlockAddress());
    Offset += MO.getOffset();
    break;

  case MachineOperand::MO_ExternalSymbol:
    Symbol = Printer.GetExternalSymbolSymbol(MO.getSymbolName());
    Offset += MO.getOffset();
    break;

  case MachineOperand::MO_JumpTableIndex:
    Symbol = Printer.GetJTISymbol(MO.getIndex());
    break;

  default:
    llvm_unreachable("<unknown operand type>");
  }

  const MCExpr *Expr = MCSymbolRefExpr::create(Symbol, Kind, Ctx);

  if (Offset) {
    // Assume offset is never negative.
    assert(Offset > 0);
    Expr =
        MCBinaryExpr::createAdd(Expr, MCConstantExpr::create(Offset, Ctx), Ctx);
  }

  if (TargetKind != M88kMCExpr::CEK_None)
    Expr = M88kMCExpr::create(TargetKind, Expr, Ctx);

  return MCOperand::createExpr(Expr);
}

void M88kMCInstLower::lower(const MachineInstr *MI, MCInst &OutMI) const {
  OutMI.setOpcode(MI->getOpcode());
  for (unsigned I = 0, E = MI->getNumOperands(); I != E; ++I) {
    const MachineOperand &MO = MI->getOperand(I);
    // Ignore all implicit register operands.
    if (!MO.isReg() || !MO.isImplicit())
      OutMI.addOperand(lowerOperand(MO));
  }
}
