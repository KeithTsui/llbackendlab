#ifndef LLVM_LIB_TARGET_M88K_M88KREGISTERINFO_H
#define LLVM_LIB_TARGET_M88K_M88KREGISTERINFO_H
#define GET_REGINFO_HEADER
#include "M88kGenRegisterInfo.inc"
namespace llvm {
struct M88kRegisterInfo : public M88kGenRegisterInfo {
  M88kRegisterInfo();

  /// Code Generation virtual methods...
  const MCPhysReg *getCalleeSavedRegs(const MachineFunction *MF) const override;
  const uint32_t *getCallPreservedMask(const MachineFunction &MF,
                                       CallingConv::ID) const override;

  BitVector getReservedRegs(const MachineFunction &MF) const override;

  void eliminateFrameIndex(MachineBasicBlock::iterator II, int SPAdj,
                           unsigned FIOperandNum,
                           RegScavenger *RS = nullptr) const override;

  Register getFrameRegister(const MachineFunction &MF) const override;


};

} // end namespace llvm

#endif
