#include "M88kTargetMachine.h"
#include "M88kISelDAGToDAG.h"
#include "TargetInfo/M88kTargetInfo.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/CodeGen/TargetLoweringObjectFileImpl.h"
#include "llvm/CodeGen/TargetPassConfig.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/TargetRegistry.h"
using namespace llvm;
extern "C" LLVM_EXTERNAL_VISIBILITY void LLVMInitializeM88kTarget() {
  RegisterTargetMachine<M88kTargetMachine> X(getTheM88kTarget());
}

namespace {
std::string computeDataLayout(const Triple &TT, StringRef CPU, StringRef FS) {
  std::string Ret;

  // little endian.
  Ret += "e";

  // Data mangling. -m:e
  Ret += DataLayout::getManglingComponent(TT);

  // Pointers are 16 bit.
  Ret += "-p:16:16";

  // Make sure that global data has at least 16 bits of alignment by
  // default, so that we can refer to it using LARL.  We don't have any
  // special requirements for stack variables though.
  Ret += "-i1:16-i8:16-i16:16";

  // Aggregate
  Ret += "-a:16";

  // Integer registers are 16bits.
  Ret += "-n16";

  // Stack Natural alignment
  Ret += "-S16";

  return Ret;
}

// TODO: Check.
Reloc::Model getEffectiveRelocModel(Optional<Reloc::Model> RM) {
  if (!RM.hasValue() || *RM == Reloc::DynamicNoPIC)
    return Reloc::Static;
  return *RM;
}

} // namespace

M88kTargetMachine::M88kTargetMachine(const Target &T, const Triple &TT,
                                     StringRef CPU, StringRef FS,
                                     const TargetOptions &Options,
                                     Optional<Reloc::Model> RM,
                                     Optional<CodeModel::Model> CM,
                                     CodeGenOpt::Level OL, bool JIT)
    : LLVMTargetMachine(T, computeDataLayout(TT, CPU, FS), TT, CPU, FS, Options,
                        getEffectiveRelocModel(RM),
                        getEffectiveCodeModel(CM, CodeModel::Medium), OL),
      TLOF(std::make_unique<TargetLoweringObjectFileELF>()) {
  initAsmInfo();
}

M88kTargetMachine::~M88kTargetMachine() {}

const M88kSubtarget *
M88kTargetMachine::getSubtargetImpl(const Function &F) const {
  Attribute CPUAttr = F.getFnAttribute("target-cpu");
  Attribute FSAttr = F.getFnAttribute("target-features");
  std::string CPU = !CPUAttr.hasAttribute(Attribute::None)
                        ? CPUAttr.getValueAsString().str()
                        : TargetCPU;
  std::string FS = !FSAttr.hasAttribute(Attribute::None)
                       ? FSAttr.getValueAsString().str()
                       : TargetFS;
  auto &I = SubtargetMap[CPU + FS];
  if (!I) {
    resetTargetOptions(F);
    I = std::make_unique<M88kSubtarget>(TargetTriple, CPU, FS, *this);
  }
  return I.get();
}

namespace {
class M88kPassConfig : public TargetPassConfig {
public:
  M88kPassConfig(M88kTargetMachine &TM, PassManagerBase &PM)
      : TargetPassConfig(TM, PM) {}
  M88kTargetMachine &getM88kTargetMachine() const {
    return getTM<M88kTargetMachine>();
  }
  bool addInstSelector() override;
  void addPreEmitPass() override;
};
} // namespace

TargetPassConfig *M88kTargetMachine::createPassConfig(PassManagerBase &PM) {
  return new M88kPassConfig(*this, PM);
}

bool M88kPassConfig::addInstSelector() {
  addPass(createM88kISelDag(getM88kTargetMachine(), getOptLevel()));
  return false;
}

void M88kPassConfig::addPreEmitPass() {}
