#ifndef LLVM_LIB_TARGET_M88K_M88KTARGETMACHINE_H
#define LLVM_LIB_TARGET_M88K_M88KTARGETMACHINE_H
#include "M88kSubtarget.h"
#include "llvm/Target/TargetLoweringObjectFile.h"
#include "llvm/Target/TargetMachine.h"
namespace llvm {
class M88kTargetMachine : public LLVMTargetMachine {
  std::unique_ptr<TargetLoweringObjectFile> TLOF;
  mutable StringMap<std::unique_ptr<M88kSubtarget>> SubtargetMap;

public:
  M88kTargetMachine(const Target &T, const Triple &TT, StringRef CPU,
                    StringRef FS, const TargetOptions &Options,
                    Optional<Reloc::Model> RM, Optional<CodeModel::Model> CM,
                    CodeGenOpt::Level OL, bool JIT);
  ~M88kTargetMachine() override;
  const M88kSubtarget *getSubtargetImpl(const Function &) const override;
  TargetPassConfig *createPassConfig(PassManagerBase &PM) override;
  TargetLoweringObjectFile *getObjFileLowering() const override {
    return TLOF.get();
  }
};

} // end namespace llvm

#endif
