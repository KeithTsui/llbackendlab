//===-- M88kFunctionInfo.cpp -*- C++ -*-===//
#include "M88kFunctionInfo.h"
#include "M88kInstrInfo.h"
#include "M88kSubtarget.h"
#include "MCTargetDesc/M88kMCTargetDesc.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/IR/Function.h"
using namespace llvm;
static bool FixGlobalBaseReg;
M88kFunctionInfo::~M88kFunctionInfo() {}
bool M88kFunctionInfo::globalBaseRegFixed() const { return FixGlobalBaseReg; }

bool M88kFunctionInfo::globalBaseRegSet() const { return GlobalBaseReg; }

unsigned M88kFunctionInfo::getGlobalBaseReg() {
  return GlobalBaseReg = M88k::R0;
}
void M88kFunctionInfo::createEhDataRegsFI() {
  const TargetRegisterInfo &TRI = *MF.getSubtarget().getRegisterInfo();
  for (int I = 0; I < 2; ++I) {
    const TargetRegisterClass &RC = M88k::GPRRegClass;
    EhDataRegFI[I] = MF.getFrameInfo().CreateStackObject(
        TRI.getSpillSize(RC), TRI.getSpillAlign(RC), false);
  }
}
MachinePointerInfo M88kFunctionInfo::callPtrInfo(const char *ES) {
  return MachinePointerInfo(MF.getPSVManager().getExternalSymbolCallEntry(ES));
}
MachinePointerInfo M88kFunctionInfo::callPtrInfo(const GlobalValue *GV) {
  return MachinePointerInfo(MF.getPSVManager().getGlobalValueCallEntry(GV));
}
void M88kFunctionInfo::anchor() {}
