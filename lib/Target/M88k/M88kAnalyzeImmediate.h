//===-- M88kAnalyzeImmediate.h -*- C++ -*-===//
#ifndef M88K_M88KANALYZEIMMEDIATE_H
#define M88K_M88KANALYZEIMMEDIATE_H
#include "llvm/ADT/SmallVector.h"
#include "llvm/Support/DataTypes.h"
namespace llvm {
class M88kAnalyzeImmediate {
public:
  struct Inst {
    unsigned Opc, ImmOpnd;
    Inst(unsigned Opc, unsigned ImmOpnd);
  };
  typedef SmallVector<Inst, 7> InstSeq;

  /// Analyze - Get an instruction sequence to load immediate Imm. The last
  /// instruction in the sequence must be an ADDiu if LastInstrIsADDiu is
  /// true;
  const InstSeq &Analyze(uint64_t Imm, unsigned Size, bool LastInstrIsADDiu);

private:
  typedef SmallVector<InstSeq, 5> InstSeqLs;

  /// AddInstr - Add I to all instruction sequences in SeqLs.
  void AddInstr(InstSeqLs &SeqLs, const Inst &I);

  /// GetInstSeqLsADDiu - Get instruction sequences which end with an ADDiu to
  /// load immediate Imm
  void GetInstSeqLsADDiu(uint64_t Imm, unsigned RemSize, InstSeqLs &SeqLs);

  /// GetInstSeqLsORi - Get instruction sequences which end with an ORi to
  /// load immediate Imm
  void GetInstSeqLsORi(uint64_t Imm, unsigned RemSize, InstSeqLs &SeqLs);

  /// GetInstSeqLsSHL - Get instruction sequences which end with a SHL to
  /// load immediate Imm
  void GetInstSeqLsSHL(uint64_t Imm, unsigned RemSize, InstSeqLs &SeqLs);

  /// GetInstSeqLs - Get instruction sequences to load immediate Imm.
  void GetInstSeqLs(uint64_t Imm, unsigned RemSize, InstSeqLs &SeqLs);

  /// GetShortestSeq - Find the shortest instruction sequence in SeqLs and
  /// return it in Insts.
  void GetShortestSeq(InstSeqLs &SeqLs, InstSeq &Insts);

  unsigned Size;
  unsigned ADDiu, ORi, SHL;
  InstSeq Insts;
};
} // namespace llvm

#endif /* M88K_M88KANALYZEIMMEDIATE_H */
