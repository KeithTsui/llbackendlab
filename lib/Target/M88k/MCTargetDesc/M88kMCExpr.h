//===-- M88kMCExpr.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef MCTARGETDESC_M88KMCEXPR_H
#define MCTARGETDESC_M88KMCEXPR_H

#include "llvm/MC/MCAsmLayout.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCValue.h"

namespace llvm {

class M88kMCExpr : public MCTargetExpr {
public:
  enum M88kExprKind {
    CEK_None,
    CEK_ABS_HI,
    CEK_ABS_LO,
    CEK_CALL_HI16,
    CEK_CALL_LO16,
    CEK_DTP_HI,
    CEK_DTP_LO,
    CEK_GOT,
    CEK_GOTTPREL,
    CEK_GOT_CALL,
    CEK_GOT_DISP,
    CEK_GOT_HI16,
    CEK_GOT_LO16,
    CEK_GPREL,
    CEK_TLSGD,
    CEK_TLSLDM,
    CEK_TP_HI,
    CEK_TP_LO,
    CEK_Special,
  };

private:
  const M88kExprKind Kind;
  const MCExpr *Expr;

  explicit M88kMCExpr(M88kExprKind Kind, const MCExpr *Expr)
      : Kind(Kind), Expr(Expr) {}

public:
  static const M88kMCExpr *create(M88kExprKind Kind, const MCExpr *Expr,
                                  MCContext &Ctx);
  static const M88kMCExpr *
  create(const MCSymbol *Symbol, M88kMCExpr::M88kExprKind Kind, MCContext &Ctx);
  static const M88kMCExpr *createGpOff(M88kExprKind Kind, const MCExpr *Expr,
                                       MCContext &Ctx);

  /// Get the kind of this expression.
  M88kExprKind getKind() const { return Kind; }

  /// Get the child of this expression.
  const MCExpr *getSubExpr() const { return Expr; }

  void printImpl(raw_ostream &OS, const MCAsmInfo *MAI) const override;
  bool evaluateAsRelocatableImpl(MCValue &Res, const MCAsmLayout *Layout,
                                 const MCFixup *Fixup) const override;
  void visitUsedExpr(MCStreamer &Streamer) const override;
  MCFragment *findAssociatedFragment() const override {
    return getSubExpr()->findAssociatedFragment();
  }

  void fixELFSymbolsInTLSFixups(MCAssembler &Asm) const override;

  static bool classof(const MCExpr *E) {
    return E->getKind() == MCExpr::Target;
  }

  bool isGpOff(M88kExprKind &Kind) const;
  bool isGpOff() const {
    M88kExprKind Kind;
    return isGpOff(Kind);
  }
};
} // namespace llvm

#endif /* MCTARGETDESC_M88KMCEXPR_H */
