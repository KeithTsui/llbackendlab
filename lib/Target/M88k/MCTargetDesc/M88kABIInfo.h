#ifndef LLVM_LIB_TARGET_M88k_MCTargetDesc_M88kABIInfo_H
#define LLVM_LIB_TARGET_M88k_MCTargetDesc_M88kABIInfo_H
#include "llvm/ADT/ArrayRef.h"
#include "llvm/IR/CallingConv.h"
#include "llvm/MC/MCRegisterInfo.h"

namespace llvm {
namespace M88kABIInfo {

/// The registers to use for byval arguments.
ArrayRef<MCPhysReg> const GetByValArgRegs();

/// The registers to use for the variable argument list.
ArrayRef<MCPhysReg> const GetVarArgRegs();

/// Obtain the size of the area allocated by the callee for arguments.
/// CallingConv::FastCall affects the value for O32.
unsigned GetCalleeAllocdArgSizeInBytes(CallingConv::ID CC);
unsigned GetStackPtr();
unsigned GetFramePtr();
unsigned GetEhDataReg(unsigned I);
int EhDataRegSize();

} // namespace M88kABIInfo

} // namespace llvm
#endif
