#ifndef LLVM_LIB_TARGET_M88K_MCTARGETDESC_M88KMCASMINFO_H
#define LLVM_LIB_TARGET_M88K_MCTARGETDESC_M88KMCASMINFO_H
#include "llvm/MC/MCAsmInfoELF.h"
namespace llvm {
class Triple;
// define asm directives and similar asm information
class M88kMCAsmInfo : public MCAsmInfoELF {
public:
  explicit M88kMCAsmInfo(const Triple &TT);
};
} // end namespace llvm

#endif
