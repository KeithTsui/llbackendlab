//===-- M88kABIInfo.cpp -*- C++ -*-===//

#include "M88kABIInfo.h"
#include "M88kMCTargetDesc.h"
#include "M88kRegisterInfo.h"

namespace llvm {
namespace M88kABIInfo {
/// The registers to use for byval arguments.
ArrayRef<MCPhysReg> const GetByValArgRegs() { return {}; }

/// The registers to use for the variable argument list.
ArrayRef<MCPhysReg> const GetVarArgRegs() { return {}; }

/// Obtain the size of the area allocated by the callee for arguments.
/// CallingConv::FastCall affects the value for O32.
unsigned GetCalleeAllocdArgSizeInBytes(CallingConv::ID CC) { return 0; }
unsigned GetStackPtr() { return M88k::SP; }
unsigned GetFramePtr() { return M88k::FP; }
unsigned GetEhDataReg(unsigned I) { return 0; }
int EhDataRegSize() { return 0; }

} // namespace M88kABIInfo
} // namespace llvm
