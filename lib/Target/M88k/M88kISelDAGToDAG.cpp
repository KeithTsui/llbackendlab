#include "M88kISelDAGToDAG.h"
#include "M88kTargetMachine.h"
#include "MCTargetDesc/M88kMCTargetDesc.h"
#include "llvm/CodeGen/SelectionDAGISel.h"
using namespace llvm;
#define DEBUG_TYPE "m88k-isel"
namespace {
class M88kDAGToDAGISel : public SelectionDAGISel {
  const M88kTargetMachine &getTargetMachine() const {
    return static_cast<const M88kTargetMachine &>(TM);
  }

public:
  M88kDAGToDAGISel(M88kTargetMachine &TM, CodeGenOpt::Level OptLevel)
      : SelectionDAGISel(TM, OptLevel) {}
  StringRef getPassName() const override {
    return "M88k DAG->DAG Pattern Instruction Selection";
  }
  // Complex Pattern.
  bool SelectFrameObject(SDNode *Parent, SDValue N, SDValue &Base,
                         SDValue &Offset);
  // Override SelectionDAGISel.
  void Select(SDNode *Node) override;
#include "M88kGenDAGISel.inc"
};
} // end anonymous namespace

FunctionPass *llvm::createM88kISelDag(M88kTargetMachine &TM,
                                      CodeGenOpt::Level OptLevel) {
  return new M88kDAGToDAGISel(TM, OptLevel);
}

bool M88kDAGToDAGISel::SelectFrameObject(SDNode *Parent, SDValue frameIndex,
                                         SDValue &Base, SDValue &Offset) {
  EVT ValTy = frameIndex.getValueType();
  SDLoc DL(frameIndex);
  // if Address is FI, get the TargetFrameIndex.
  if (FrameIndexSDNode *FIN = dyn_cast<FrameIndexSDNode>(frameIndex)) {
    Base = CurDAG->getTargetFrameIndex(FIN->getIndex(), ValTy);
    Offset = CurDAG->getTargetConstant(0, DL, ValTy);
    return true;
  }

  if (CurDAG->isBaseWithConstantOffset(frameIndex)) {
    ConstantSDNode *CN = dyn_cast<ConstantSDNode>(frameIndex.getOperand(1));
    if (isInt<16>(CN->getSExtValue())) {
      // If the first operand is a FI, get the TargetFI Node
      if (FrameIndexSDNode *FIN =
              dyn_cast<FrameIndexSDNode>(frameIndex.getOperand(0)))
        Base = CurDAG->getTargetFrameIndex(FIN->getIndex(), ValTy);
      else
        Base = frameIndex.getOperand(0);

      Offset = CurDAG->getTargetConstant(CN->getZExtValue(), DL, ValTy);
      return true;
    }
  }

  Base = frameIndex;
  Offset = CurDAG->getTargetConstant(0, DL, ValTy);
  return true;
}

void M88kDAGToDAGISel::Select(SDNode *Node) {
  SDLoc dl(Node);
  // If we have a custom node, we already have selected!
  if (Node->isMachineOpcode()) {
    Node->setNodeId(-1);
    return;
  }
  switch (Node->getOpcode()) {
  default:
    break;
  }
  // default instruction selection
  SelectCode(Node);
}
