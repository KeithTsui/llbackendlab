#include "M88kFrameLowering.h"
#include "M88kInstrInfo.h"
#include "M88kSubtarget.h"
#include "MCTargetDesc/M88kMCTargetDesc.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineModuleInfo.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/TargetInstrInfo.h"
#include "llvm/Target/TargetMachine.h"
#include <_types/_uint64_t.h>

using namespace llvm;
M88kFrameLowering::M88kFrameLowering()
    : TargetFrameLowering(
          TargetFrameLowering::StackGrowsDown, Align(2) /* Stack alignment*/,
          0 /*local area offset*/, Align(2) /*transient stack alignment*/,
          false /* StackRealignable */) {}

/// Mark \p Reg and all registers aliasing it in the bitset.
static void setAliasRegs(MachineFunction &MF, BitVector &SavedRegs,
                         unsigned Reg) {
  const TargetRegisterInfo *TRI = MF.getSubtarget().getRegisterInfo();
  for (MCRegAliasIterator AI(Reg, TRI, true); AI.isValid(); ++AI)
    SavedRegs.set(*AI);
}

// This method is called immediately before PrologEpilogInserter scans the
//  physical registers used to determine what callee saved registers should be
//  spilled. This method is optional.
void M88kFrameLowering::determineCalleeSaves(MachineFunction &MF,
                                             BitVector &SavedRegs,
                                             RegScavenger *RS) const {
  TargetFrameLowering::determineCalleeSaves(MF, SavedRegs, RS);
  unsigned FP = M88k::FP;
  if (hasFP(MF))
    setAliasRegs(MF, SavedRegs, FP);
  if (MF.getFrameInfo().hasCalls())
    setAliasRegs(MF, SavedRegs, M88k::LR);
  return;
}

bool M88kFrameLowering::spillCalleeSavedRegisters(
    MachineBasicBlock &MBB, MachineBasicBlock::iterator MI,
    ArrayRef<CalleeSavedInfo> CSI, const TargetRegisterInfo *TRI) const {
  MachineFunction *MF = MBB.getParent();
  MachineBasicBlock *EntryBlock = &MF->front();
  const TargetInstrInfo &TII = *MF->getSubtarget().getInstrInfo();
  for (unsigned i = 0, e = CSI.size(); i != e; ++i) {
    // Add the callee-saved register as live-in.
    // Do not add if the register is LR and return address is taken,
    // because it has already been added in method
    // Cpu0TargetLowering::LowerRETURNADDR. It's killed at the spill, unless the
    // register is LR and return address is taken.
    unsigned Reg = CSI[i].getReg().id();
    bool IsRAAndRetAddrIsTaken =
        (Reg == M88k::LR) && MF->getFrameInfo().isReturnAddressTaken();
    if (!IsRAAndRetAddrIsTaken)
      EntryBlock->addLiveIn(Reg);
    // Insert the spill to the stack frame.
    bool IsKill = !IsRAAndRetAddrIsTaken;
    const TargetRegisterClass *RC = TRI->getMinimalPhysRegClass(Reg);
    TII.storeRegToStackSlot(*EntryBlock, MI, Reg, IsKill, CSI[i].getFrameIdx(),
                            RC, TRI);
  }
  return true;
}
// Because callee saved registers are already handled in
// spillCalleeSavedRegisters()
// 1. adjust stack size, i.e. Stack pointer register.
// 2. update frame pointer register
void M88kFrameLowering::emitPrologue(MachineFunction &MF,
                                     MachineBasicBlock &MBB) const {
  MachineFrameInfo &MFI = MF.getFrameInfo();
  auto &STI = MF.getSubtarget();
  const M88kInstrInfo &TII =
      *static_cast<const M88kInstrInfo *>(STI.getInstrInfo());
  MachineBasicBlock::iterator MBBI = MBB.begin();
  DebugLoc dl = MBBI != MBB.end() ? MBBI->getDebugLoc() : DebugLoc();

  // Adjust stack, enlarge stack
  uint64_t StackSize = MFI.getStackSize();
  if (StackSize == 0 && !MFI.adjustsStack())
    return;
  uint64_t stackGrowth =
      getStackGrowthDirection() == StackDirection::StackGrowsDown ? -StackSize
                                                                  : StackSize;
  TII.adjustStackPtr(M88k::SP, stackGrowth, MBB, MBBI);

  // if framepointer enabled, set it to point to the stack pointer.
  if (hasFP(MF)) {
    const std::vector<CalleeSavedInfo> &CSI = MFI.getCalleeSavedInfo();
    if (!CSI.empty()) {
      for (unsigned i = 0; i < CSI.size(); ++i)
        ++MBBI;
    }
    //@ Insert instruction "move $fp, $sp" at this location.
    BuildMI(MBB, MBBI, dl, TII.get(M88k::MVR), M88k::FP)
        .addReg(M88k::SP)
        .setMIFlag(MachineInstr::FrameSetup);
  }
}

// Because callee saved registers are already handled in
// spillCalleeSavedRegisters()
// 1. update frame pointer register
// 2. adjust stack size, i.e. Stack pointer register.
void M88kFrameLowering::emitEpilogue(MachineFunction &MF,
                                     MachineBasicBlock &MBB) const {

  MachineBasicBlock::iterator MBBI = MBB.getFirstTerminator();
  MachineFrameInfo &MFI = MF.getFrameInfo();
  auto &STI = MF.getSubtarget();
  const M88kInstrInfo &TII =
      *static_cast<const M88kInstrInfo *>(STI.getInstrInfo());
  DebugLoc DL = MBBI != MBB.end() ? MBBI->getDebugLoc() : DebugLoc();

  // if framepointer enabled, set it to point to the stack pointer.
  if (hasFP(MF)) {
    // Find the first instruction that restores a callee-saved register.
    MachineBasicBlock::iterator I = MBBI;
    for (unsigned i = 0; i < MFI.getCalleeSavedInfo().size(); ++i)
      --I;
    //@ Insert instruction "move $fp, $sp" at this location.
    BuildMI(MBB, I, DL, TII.get(M88k::MVR), M88k::SP)
        .addReg(M88k::FP)
        .setMIFlag(MachineInstr::FrameSetup);
  }

  // Adjust stack, shrink stack
  uint64_t StackSize = MFI.getStackSize();
  if (StackSize == 0 && !MFI.adjustsStack())
    return;
  uint64_t stackGrowth =
      getStackGrowthDirection() == StackDirection::StackGrowsDown ? -StackSize
                                                                  : StackSize;
  TII.adjustStackPtr(M88k::SP, -stackGrowth, MBB, MBBI);
}

bool M88kFrameLowering::hasFP(const MachineFunction &MF) const {
  // always has frame pointer now
  return true;

  // in some cases, we can use frame pointer as general register,
  // if frame pointer is not neccessory.

  // const MachineFrameInfo &MFI = MF.getFrameInfo();
  // const TargetRegisterInfo *TRI = MF.getSubtarget().getRegisterInfo();

  // return MF.getTarget().Options.DisableFramePointerElim(MF) ||
  //        MFI.hasVarSizedObjects() || MFI.isFrameAddressTaken() ||
  //        TRI->needsStackRealignment(MF);
}

// Eliminate ADJCALLSTACKDOWN, ADJCALLSTACKUP pseudo instructions
MachineBasicBlock::iterator M88kFrameLowering::eliminateCallFramePseudoInstr(
    MachineFunction &MF, MachineBasicBlock &MBB,
    MachineBasicBlock::iterator I) const {
  auto const &STI = static_cast<M88kSubtarget const &>(MF.getSubtarget());
  unsigned SP = M88k::SP;
  if (!hasReservedCallFrame(MF)) {
    int64_t Amount = I->getOperand(0).getImm();
    if (I->getOpcode() == M88k::ADJCALLSTACKDOWN)
      Amount = -Amount;
    STI.getInstrInfo()->adjustStackPtr(SP, Amount, MBB, I);
  }
  return MBB.erase(I);
}
