//===-- M88kISelDAGToDAG.h -*- C++ -*-===//
#ifndef LLVM_LIB_TARGET_M88K_M88KISELDAGTODAG_H
#define LLVM_LIB_TARGET_M88K_M88KISELDAGTODAG_H
#include "llvm/Support/CodeGen.h"
namespace llvm {
  class M88kTargetMachine;
  class FunctionPass;
  FunctionPass *createM88kISelDag(M88kTargetMachine &TM,
                                  CodeGenOpt::Level OptLevel);
} // end namespace llvm

#endif /* LLVM_LIB_TARGET_M88K_M88KISELDAGTODAG_H */
