#!/usr/bin/env bash

#clean up LCC target
cd ~/LLBackendLab/llvm-project/
git reset --hard 2ebaacc494d7e20ccf119038a23af46d54ab2386
git clean -xdf

#copy LCC backend code
cp -rf ~/LLBackendLab/llvm/lib/Target/LCC ~/LLBackendLab/llvm-project/llvm/lib/Target/

#if build succeeds, then output patch
cmake -DCMAKE_BUILD_TYPE=Debug \
      -DLLVM_TARGETS_TO_BUILD='LCC' \
      -DLLVM_PARALLEL_COMPILE_JOBS=4 -DLLVM_PARALLEL_LINK_JOBS=1 -G "Ninja" -B build -S llvm && \
    cmake --build build && \
    git add . && git commit -m "make patch" && \
    git diff fed41342a82f5a3a9201819a82bf7a48313e296b  > ~/LLBackendLab/llvm/patches/lcc.patch
